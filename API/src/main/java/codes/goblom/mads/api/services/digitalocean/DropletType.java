/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.services.digitalocean;

import codes.goblom.mads.api.ServerType;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Data;
/**
 *
 * @author Bryan Larson
 */
@Data
@Builder
public class DropletType implements ServerType {
    
    final String name;
    String region;
    String operatingSystem;
    String ram;
    List<String> initCmds;
    CloudInit cloudInit;
    
    boolean enableBackup;
    boolean enableIpv6;
    boolean enablePrivateNetworking;
    
    public void addInitCmd(String cmd) {
        if (this.initCmds == null) {
            this.initCmds = new ArrayList();
        }
        
        initCmds.add(cmd);
    }
}
