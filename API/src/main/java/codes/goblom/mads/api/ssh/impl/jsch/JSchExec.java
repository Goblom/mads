/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.ssh.impl.jsch;

import codes.goblom.mads.api.ssh.SSHController;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import lombok.Getter;
import codes.goblom.mads.api.ssh.wrappers.WrappedExec;

/**
 *
 * @author Bryan Larson
 */
public class JSchExec implements WrappedExec {
    
    @Getter
    private Session session;
    
    @Getter
    private List<String> latestOutput;
    
    @Override
    public void attach(SSHController controller) {
        if (controller instanceof JSchSSHController) {
            if (session == null) {
                session = ((JSchSSHController) controller).getSession();
            }
        }
    }
    
    @Override
    public void close(SSHController controller) {
        this.session = null;
        this.latestOutput = null;
    }
    
    @Override
    public boolean isReady() {
        return getSession() != null && getSession().isConnected();
    }
    
    @Override
    public void exec(String exec) {
        if (!isReady()) {
            throw new RuntimeException("WrappedExec not opened correctly.");
        }
        
        ChannelExec channel = null;
        
        try {
            channel = (ChannelExec) session.openChannel("exec");
            channel.setCommand(exec);
            channel.setInputStream(null);
            channel.connect();
            channel.disconnect();
        } catch (JSchException e) { 
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public List<String> execOutputOf(String exec) {
        if (!isReady()) {
            throw new RuntimeException("WrappedExec not opened correctly.");
        }
        
        ChannelExec channel = null;
        
        try {
            channel = (ChannelExec) session.openChannel("exec");
            channel.setCommand(exec);
            channel.setInputStream(null);
        } catch (JSchException e) { 
            throw new RuntimeException(e);
        }
        
        try {
            InputStream in = channel.getInputStream();
            channel.connect();

            StringBuilder ret = new StringBuilder();
            byte[] buffer = new byte[1024];

            boolean close = false;
            while (!close) {
                while (in.available() > 0) {
                    int i = in.read(buffer, 0, 1024);
                    if (i < 0) break;
                    String found = new String(buffer, 0, i);
                    ret.append(found);
//                    if (channel.isClosed()) break;

                    if (found.contains("Starting minecraft server version")) { //prevent continuous loop from server log...
                        close = true;
                        break;
                    }
                }
                
                if (channel.isClosed()) break;
            }
            
            channel.disconnect();
            return this.latestOutput = Arrays.asList(ret.toString().split("\n"));
        } catch (JSchException | IOException e) {
            throw new RuntimeException(e);
        }
    }
}
