/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin.callbacks;

import codes.goblom.mads.api.ssh.UploadProgressMonitor.UploadStatus;
import codes.goblom.mads.api.ssh.impl.jsch.JSchProgressMonitor;
import codes.goblom.mads.plugin.Permissions;
import codes.goblom.mads.plugin.utils.Messenger;
import lombok.RequiredArgsConstructor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 *
 * @author Bryan Larson
 */
@RequiredArgsConstructor
@Deprecated //TODO: Fix for use with non JSchProgressMonitor
public class SenderUploadProgressMonitor extends JSchProgressMonitor {

    private final CommandSender sender;
    private long previousPercent = 0;
    
    @Override
    public void accept(UploadStatus t) {
        switch (t) {
            case STARTED:
                Messenger.sendMessage(sender, "Upload Started...");
                for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
                    if (sender == player) continue;
                    if (Permissions.NOTIFY.has(sender) || Permissions.NOTIFY_UPLOAD.has(sender)) {
                        Messenger.sendMessage(player, "Started upload of file... " + getFile().getName());
                    }
                }
                break;
            case FINISHED:
                Messenger.sendMessage(sender, "Upload Finished...");
                for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
                    if (sender == player) continue;
                    if (Permissions.NOTIFY.has(sender) || Permissions.NOTIFY_UPLOAD.has(sender)) {
                        Messenger.sendMessage(player, "Finished upload of file... " + getFile().getName());
                    }
                }
                break;
            case TICK:
                long percent = getPercent();
                long finished = getCount();
                long max = getMax();
                String fileName = getFile().getName();
                
                if (previousPercent == percent) return;
                this.previousPercent = percent;
                
                long kbFinished = finished / 1024;
                long kbMax = max / 1024;
                Messenger.sendMessage(sender, "Uploading [" + fileName + "] " + kbFinished + "kb/" + kbMax + "kb (" + percent + "%)");
                break;
        }
    }
    
}
