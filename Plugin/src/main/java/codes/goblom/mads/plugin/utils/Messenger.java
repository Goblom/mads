/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin.utils;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;

/**
 *
 * @author Bryan Larson
 */
public class Messenger {
    
    private static ComponentBuilder PREFIX;
    
    public static void setPrefix(ComponentBuilder builder) {
        PREFIX = builder;
    }
    
    public static void sendMessage(CommandSender sender, String message) {
        ComponentBuilder cb = new ComponentBuilder(PREFIX);
        cb.append(" ").reset();
        cb.append(new TextComponent(message));
        sender.sendMessage(cb.create());
    }
    
    public static void sendMessage(CommandSender sender, String message, ChatColor... colors) {
        ComponentBuilder cb = new ComponentBuilder(PREFIX);
        cb.append(" ").reset();
        cb.append(new TextComponent(message));
        
        for (ChatColor color : colors) {
            switch (color) {
                case STRIKETHROUGH:
                    cb.strikethrough(true);
                    break;
                case UNDERLINE:
                    cb.underlined(true);
                    break;
                case MAGIC:
                    cb.obfuscated(true);
                    break;
                case BOLD:
                    cb.bold(true);
                    break;
                case ITALIC:
                    cb.italic(true);
                    break;
                case RESET:
                    cb.reset();
                    break;
                default:
                    cb.color(color);
                    break;
            }
                        
        }
        sender.sendMessage(cb.create());
    }
    
    public static void sendMessage(CommandSender sender, ComponentBuilder builder) {
        ComponentBuilder cb = new ComponentBuilder(PREFIX);
        cb.append(" ").reset();
        cb.append(builder.create());
        sender.sendMessage(cb.create());
    }
}
