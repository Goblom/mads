/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin.services.local;

import codes.goblom.mads.api.MadsApi;
import codes.goblom.mads.api.ServerBuilder;
import codes.goblom.mads.api.ServerController;
import codes.goblom.mads.api.ServerType;
import codes.goblom.mads.api.events.MadsCreateEvent;
import codes.goblom.mads.api.services.DefaultServerType;
import codes.goblom.mads.api.services.local.LocalType;
import codes.goblom.mads.plugin.MadsBungeePlugin;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.config.Configuration;

/**
 *
 * @author Bryan Larson
 */
public final class LMadsImpl extends MadsApi<LServerController> {

    private static final String CONFIG_FILE = "local.yml";
    
    private final MadsBungeePlugin plugin;
    private final Configuration config;
    
    private final List<ServerType> localTypes = new ArrayList();
    private final List<LServerController> localControllers = new ArrayList();
    
    protected static int PORT_SCAN_START;
    protected static int PORT_SCAN_END;
    
    public LMadsImpl(MadsBungeePlugin plugin) throws IOException {
        this.plugin = plugin;
        this.config = plugin.getConfiguration(CONFIG_FILE);
        
        PORT_SCAN_START = config.getInt("Port Scanner.Start");
        PORT_SCAN_END = config.getInt("Port Scanner.End");
        
        reload();
    }
    
    @Override
    public void reload() {        
        this.localTypes.clear();
        
        List<Map<String, Object>> maps = (List<Map<String, Object>>) config.getList("Server Types");
        
        maps.stream().map((map) -> {
            LocalType.LocalTypeBuilder builder = LocalType.builder();
            
            builder.name(map.get("name").toString().replace(" ", "_"));
            
            String launchArgs;
            if (map.containsKey("launch args")) {
                launchArgs = map.get("launch args").toString();
                
                if (launchArgs == null || launchArgs.isEmpty()) {
                    launchArgs = config.get("Default.Launch Args").toString();
                }
            } else {
                launchArgs = config.get("Default.Launch Args").toString();
            }
            
            
            builder.launchArgs(launchArgs);
            
            Object init = map.get("Init");
            
            if (init != null) {
                if (init instanceof List) {
                    builder.initCmds(Lists.transform((List) init, Object::toString));
                } else {
                    builder.initCmds(Arrays.asList(init.toString()));
                }
            }
            
            return builder.build();
        }).forEachOrdered(this::addServerType);
    }

    @Override
    public boolean addServerType(ServerType type) {
        if (!(type instanceof LocalType) && !(type instanceof DefaultServerType)) {
            throw new RuntimeException("Mads LocalAPI supports LocalServerType & BasicServerType not " + type.getClass().getName());
        }
        
        if (type.getName().contains(" ")) {
            throw new UnsupportedOperationException("Droplet type [" + type.getName() + "] cannot contain any spaces.");
        }
        
        if (!this.localTypes.stream().noneMatch((installed) -> (installed.getName().equalsIgnoreCase(type.getName())))) {
            return false;
        }
        
        return localTypes.add((LocalType) type);
    }

    @Override
    public List<ServerType> getServerTypes() {
        return new ArrayList(localTypes);
    }

    @Override
    public ServerType getServerType(String name) {
        return localTypes.stream().filter((i) -> i.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    @Override
    public void removeServerType(String name) {
        this.localTypes.stream().filter((installed) -> (installed.getName().equalsIgnoreCase(name))).forEachOrdered((installed) -> {
            this.localTypes.remove(installed);
        });
    }

    @Override
    public LServerController getControllerById(String id) {
        return getControllerByName(id);
    }

    @Override
    public LServerController getControllerByName(String name) {
        return localControllers.stream().filter((c) -> c.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    @Override
    public void removeController(String name) {
        LServerController cont = null;
        
        try {
             cont = getControllerByName(name);
             cont.destroy();
        } catch (Exception e) { }
        
        if (cont != null) {
            localControllers.remove(cont);
        }
    }

    @Override
    public Collection<LServerController> getAllControllers() {
        return Collections.unmodifiableCollection(localControllers);
    }

    @Override
    protected LServerController createController(ServerBuilder sb) {
        MadsCreateEvent event;
        
        if (!sb.getId().equals(ServerController.NOT_CREATED_ID)) {
            event = new MadsCreateEvent(sb.getId(), sb.getType(), true);
        } else {
            event = new MadsCreateEvent(sb.getName(), sb.getType(), false);
        }
        
        ProxyServer.getInstance().getPluginManager().callEvent(event);
        
        if (event.isCancelled()) {
            throw new RuntimeException(String.format("LocalServer creation of [%s] was cancelled...", event.getName().equals(ServerController.NOT_CREATED_ID) ? event.getId() : event.getName()));
        }
        
        String name = event.getName().equalsIgnoreCase(ServerController.NOT_CREATED_ID) ? event.getId() : event.getName();
        ServerType type = getServerType(event.getType());
        
        if (type == null) {
            throw new RuntimeException("ServerType " + event.getName() + " does not exist.");
        }
        
        LServerController cont = new LServerController(plugin, this, name, type);
        
        cont.create();
        cont.connect(); //Do we really need to call this for LServerController?
        
        localControllers.add(cont);
        return cont;
    }
}
