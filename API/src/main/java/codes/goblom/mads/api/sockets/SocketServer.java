/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.sockets;

import codes.goblom.mads.api.utils.ThreadStarter;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Bryan Larson
 */
public class SocketServer implements Runnable {
    
    public static void main(String[] args) throws IOException {
        int port = 5556;
        SocketServer server = new SocketServer(port);
        ThreadStarter.start(server);
        server.setReciever((conn, rec) -> { 
            System.out.println("Received: " + rec); 
            conn.send("Got your object.");
        });
        
        System.out.println("Server started...");
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Type anything to quit.");
        sc.next();
        server.stop();
    }
    
    private final ServerSocket serverSocket;
    protected SocketReceiver handler;
    
    private final List<SocketConnection> connectionThreads = new ArrayList();
    
    public SocketServer(int port) throws IOException {
        this.serverSocket = new ServerSocket(port);
    }
    
    public SocketServer(int port, SocketReceiver handler) throws IOException {
        this(port);
        
        this.handler = handler;
    }
    
    public SocketServer(String host, int port) throws IOException {
        this.serverSocket = new ServerSocket();
        this.serverSocket.bind(new InetSocketAddress(host, port));
    }
    
    public SocketServer(String host, int port, SocketReceiver handler) throws IOException {
        this(host, port);
        
        this.handler = handler;
    }
    
    public final void setReciever(SocketReceiver handler) {
        this.handler = handler;
    }
    
    public final boolean isClosed() {
        return serverSocket == null || serverSocket.isClosed();
    }
    
    public final void stop() {
        if (!isClosed()) {
            connectionThreads.forEach(SocketConnection::close);
            
            try {
                serverSocket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    @Override
    public final void run() {
        while (!isClosed()) {
            for (int i = 0; i < connectionThreads.size(); i++) {
                SocketConnection thread = connectionThreads.get(i);
                
                if (thread.isClosed()) {
                    connectionThreads.remove(i);
                }
            }
            
            try {
                Socket socket = serverSocket.accept();
                SocketConnection thread = new SocketConnection(socket, this);
                connectionThreads.add(thread);
                ThreadStarter.start(thread);
                onConnect(thread);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public final List<SocketConnection> getConnections() {
        return Collections.unmodifiableList(connectionThreads);
    }
    
    public void onConnect(SocketConnection conn) { }
}
