/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.events;

import codes.goblom.mads.api.Mads;
import codes.goblom.mads.api.ServerController;
import codes.goblom.mads.api.ServerType;
import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.plugin.Cancellable;
import net.md_5.bungee.api.plugin.Event;

/**
 *
 * @author Bryan Larson
 */
public class MadsCreateEvent extends Event implements Cancellable {

    @Getter
    private String name;
    
    @Getter
    private String id;
    
    @Getter
    @Setter
    private String type;
    
    @Getter
    @Setter
    private boolean cancelled = false;
    
    public MadsCreateEvent(String name, String type, boolean id) {
        this.type = type;
        
        if (id) {
            this.id = name;
            this.name = ServerController.NOT_CREATED_ID;
        } else {
            this.name = name;
            this.id = ServerController.NOT_CREATED_ID;
        }
    }
    
    public ServerType getServerType() {
        return Mads.getServerType(name);
    }
}
