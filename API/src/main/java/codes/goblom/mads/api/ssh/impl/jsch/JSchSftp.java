/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.ssh.impl.jsch;

import codes.goblom.mads.api.ssh.SSHController;
import codes.goblom.mads.api.ssh.UploadProgressMonitor;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import lombok.Getter;
import lombok.Setter;
import codes.goblom.mads.api.ssh.wrappers.WrappedSftp;
import com.jcraft.jsch.SftpProgressMonitor;

/**
 *
 * @author Bryan Larson
 */
public class JSchSftp implements WrappedSftp {

    @Getter
    private Session session;
    private JSchSSHController controller;
    
    @Setter
    private String uploadPath;
    
    public JSchSftp() { }
    public JSchSftp(String uploadPath) {
        this.uploadPath = uploadPath;
    }
    
    @Override
    public void attach(SSHController controller) {
        if (controller instanceof JSchSSHController) {
            if (this.controller == null) {
                this.controller = (JSchSSHController) controller;
            }

            if (session == null) {
                this.session = ((JSchSSHController) controller).getSession();
            }
        }
    }

    @Override
    public void close(SSHController controller) {
        this.session = null;
        this.controller = null;
    }
    
    @Override
    public boolean isReady() {
        return getSession() != null && getSession().isConnected();
    }
    
    @Override
    public void upload(File file, UploadProgressMonitor monitor) {
        upload(file, null, monitor);
    }
    
    @Override
    public void upload(File file, String dir, UploadProgressMonitor monitor) {
        if (!isReady()) {
            throw new RuntimeException("WrappedSftp not opened correctly.");
        }
        
        ChannelSftp sftp = null;
        
        try {
            sftp = (ChannelSftp) session.openChannel("sftp");
            sftp.connect();
        } catch (JSchException e) {
            throw new RuntimeException(e);
        }
        
        if (dir != null && !dir.isEmpty()) {
            try {
                if (uploadPath == null || uploadPath.isEmpty()) {
                    this.uploadPath = sftp.getHome(); //Default to home path if no path is designated.
                }
                
                String dirPath = uploadPath + (uploadPath.endsWith("/") ? "" : "/") + dir;
                controller.exec("mkdir -p " + dirPath);
                sftp.cd(dirPath);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        try {
            FileInputStream stream = new FileInputStream(file);
            
            if (monitor != null) {
                monitor.setFile(file);

                if (monitor instanceof SftpProgressMonitor) {
                    sftp.put(stream, file.getName(), (SftpProgressMonitor) monitor); //Only support SftpUploadProgressMonitor with JSchSftp
                } else {
                    sftp.put(stream, file.getName());
                }
            } else {
                sftp.put(stream, file.getName());
            }
        } catch (SftpException | FileNotFoundException ex) {
            throw new RuntimeException(ex);
        }

        sftp.disconnect();
    }

    @Override
    public void upload(File file) {
        upload(file, null, null);
    }

    @Override
    public void upload(File file, String dir) {
        upload(file, dir, null);
    }
}
