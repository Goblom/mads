/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin.exec;

import codes.goblom.mads.plugin.utils.Messenger;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 *
 * @author Bryan Larson
 */
public class CommandContext {
    
    @Getter
    private final CommandSender sender;
    @Getter
    private final Executor executor;
    @Getter
    private final String[] args;
    
    private List<String> tabComplete;
    @Getter 
    final boolean tabExecutor;
    
    CommandContext(Executor exec, CommandSender sender, String[] args) {
        this.executor = exec;
        this.sender = sender;
        this.args = args;
        this.tabExecutor = false;
    }
    
    CommandContext(Executor exec, CommandSender sender, String[] args, boolean executing) {
        this.sender = sender;
        this.executor = exec;
        this.args = Arrays.copyOfRange(args, 1, args.length); //Dont have start arg... that is origination
        this.tabExecutor = !executing;
    }
    
    public List<String> getTabComplete() {
        if (tabComplete == null) return null;
        
        if (sender instanceof ProxiedPlayer) {
            return tabComplete;
        }
        
        return Lists.transform(tabComplete, ChatColor::stripColor);
    }
    
    public boolean hasArg(int i) {
        try {
            return args[i] != null && !args[i].trim().isEmpty();
        } catch (Exception e) {
            return false;
        }
    }
    
    public String getArg(int i) {
        try {
            return ChatColor.stripColor(args[i]);
        } catch (Exception e) {
            return null; //return null instead of exception
        }
    }
    
    public String combineRemaining(int start) {
        if (!hasArg(start)) {
            return null;
        }
        
        StringBuilder sb = new StringBuilder();

        for (int i = start; i < args.length; i++) {
            sb.append(getArg(i)).append(" ");
        }

        return sb.toString().trim();
    }
    
    public String getArgs(int start, int end) {
        if (!hasArg(start) || !hasArg(end)) {
            return null;
        }
        
        StringBuilder sb = new StringBuilder();

        for (int i = start; i < args.length && i != end; i++) {
            sb.append(getArg(i)).append(" ");
        }

        return sb.toString().trim();
    }
    
    public int argsLength() {
        return args.length;
    }
    
    public void suggest(String suggestion) {
        if (tabComplete == null) {
            this.tabComplete = new ArrayList();
        }
        
        tabComplete.add(suggestion + ChatColor.RESET);
    }
    
    //Style goes before color
    public void suggest(String suggestion, ChatColor... colors) {
        if (tabComplete == null) {
            this.tabComplete = new ArrayList();
        }
        
        for (ChatColor color : colors) {
            if (color == null) continue;
            
            suggestion = color + "" + suggestion;
        }
        
        suggestion = ChatColor.translateAlternateColorCodes('&', suggestion);
        tabComplete.add(suggestion + ChatColor.RESET);
    }
    
    public void message(String message, ChatColor... colors) {
//        System.out.println("isTabExecutor = " + isTabExecutor());
        if (isTabExecutor()) {
            suggest(message, colors);
            return;
        }
        
        Messenger.sendMessage(sender, message, colors);
    }
    
    public void message(String message) {
//        System.out.println("isTabExecutor = " + isTabExecutor());
        if (isTabExecutor()) {
            suggest(message);
            return;
        }
        
        Messenger.sendMessage(sender, message);
    }
}
