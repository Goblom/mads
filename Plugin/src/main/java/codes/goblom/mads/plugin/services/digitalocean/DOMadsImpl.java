/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin.services.digitalocean;

import codes.goblom.mads.api.ServerBuilder;
import codes.goblom.mads.api.services.digitalocean.DropletController;
import codes.goblom.mads.api.MadsApi;
import codes.goblom.mads.api.ServerController;
import codes.goblom.mads.api.ServerType;
import codes.goblom.mads.api.auth.Authentication;
import codes.goblom.mads.api.auth.PlainTextAuth;
import codes.goblom.mads.api.events.MadsCreateEvent;
import codes.goblom.mads.api.services.DefaultServerType;
import codes.goblom.mads.api.services.digitalocean.CloudInit;
import codes.goblom.mads.api.services.digitalocean.DropletType;
import codes.goblom.mads.plugin.MadsBungeePlugin;
import codes.goblom.mads.plugin.auth.SecureAuth;
import codes.goblom.mads.plugin.services.digitalocean.cloudinit.YamlCloudInit;
import com.myjeeva.digitalocean.DigitalOcean;
import com.myjeeva.digitalocean.impl.DigitalOceanClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import lombok.AccessLevel;
import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.config.Configuration;

/**
 *
 * @author Bryan Larson
 */
public class DOMadsImpl extends MadsApi<DOControllerImpl> {

    private static final String CONFIG_FILE = "digitalocean.yml";
    
    private final MadsBungeePlugin plugin;
    private final Configuration config;
    
    @Getter(value = AccessLevel.PROTECTED)
    private final DigitalOcean backendClient;
    private final List<DropletType> dropletTypes;
    private final List<DOControllerImpl> controllers;
    
    private Authentication auth;
    
    public DOMadsImpl(MadsBungeePlugin plugin) throws IOException {
        this.plugin = plugin;
        this.config = plugin.getConfiguration(CONFIG_FILE);
        
        this.backendClient = new DigitalOceanClient(config.getString("Auth Token"));
        
        this.dropletTypes = new CopyOnWriteArrayList();
        this.controllers = new CopyOnWriteArrayList();
        
        reload();
        
//        CloudInit init = YamlCloudInit.loadFromFile(plugin, "cloud-init.yml");
//        
//        DropletType.DropletTypeBuilder tb = DropletType.builder();
//        tb.name("Survival");
//        tb.ram("4gb");
//        tb.region("sfo2");
//        tb.operatingSystem("ubuntu-16-04-x64");
//        tb.cloudInit(init);
//        tb.initCmds(Arrays.asList("@upload server.zip", "w:1 unzip server.zip", "w:15 java -jar spigot.jar", "w:60 ls"));
//        DropletType type = tb.build();
//        
//        Mads.addServerType(type);
//        
//        ServerBuilder db = Mads.buildController();
//        db.setName("Test-Droplet");
//        db.setType("Survival");
//        
//        ServerController dropletController = db.build();
    }

    @Override
    public final void reload() {
        this.dropletTypes.clear();
        
        List<Map<String, Object>> maps = (List<Map<String, Object>>) config.getList("Droplet Types");
        maps.stream().map((map) -> {
            DropletType.DropletTypeBuilder builder = DropletType.builder();
            builder.name(map.get("name").toString().replace(" ", "_")); //TODO: Support spaces in name eventually
            builder.region(map.get("region").toString());
            builder.ram(map.get("ram").toString());
            builder.operatingSystem(map.get("os").toString());
            boolean enableBackup = Boolean.getBoolean(map.get("enable-backup").toString());
            boolean enableIpv6 = Boolean.getBoolean(map.get("enable-ipv6").toString());
            boolean enablePrivateNetworking = Boolean.getBoolean(map.get("enable-private-networking").toString());
            builder.enableBackup(enableBackup);
            builder.enableIpv6(enableIpv6);
            builder.enablePrivateNetworking(enablePrivateNetworking);

            try {
                if (map.containsKey("cloud-config") && map.get("cloud-config") != null) {
                    CloudInit init = YamlCloudInit.loadFromFile(plugin, map.get("cloud-config").toString());
                    builder.cloudInit(init);
                } else {
                    builder.cloudInit(YamlCloudInit.loadFromFile(plugin, config.getString("Default.Cloud Init")));
                }
            } catch (Exception e) {
                builder.cloudInit(YamlCloudInit.loadFromFile(plugin, config.getString("Default.Cloud Init")));
            }

            if (map.containsKey("Init")) {
                List<String> list = (List<String>) map.get("Init");
                builder.initCmds(list);
            }
            return builder.build();
        }).forEachOrdered(this::addServerType);
    }
    
    @Override
    public boolean addServerType(ServerType type) {
        
        // If DefaultServerType create DropletType with minimum requirements in 
        //     order to start minecraft
        // TODO: Eventually use Most common value from Droplet Types in config...
        if (type instanceof DefaultServerType) {
            DropletType.DropletTypeBuilder convert = DropletType.builder();
            
            convert.name(type.getName());
            convert.initCmds(type.getInitCmds());
            convert.region("sfo2");
            convert.operatingSystem("ubuntu-16-04-x64");
            convert.ram("4gb");
            
            type = convert.build();
        }
        
        if (!(type instanceof DropletType)) {
            throw new RuntimeException("Mads DigitalOcean API only supports DropletType not " + type.getClass().getName());
        }
        
        final DropletType added = (DropletType) type;
        
        if (type.getName().contains(" ")) {
            throw new UnsupportedOperationException("Droplet type [" + type.getName() + "] cannot contain any spaces.");
        }
        
        if (!this.dropletTypes.stream().noneMatch((installed) -> (installed.getName().equalsIgnoreCase(added.getName())))) {
            return false;
        }
        
        return dropletTypes.add(added);
    }

    @Override
    public List<ServerType> getServerTypes() {
        return new ArrayList(dropletTypes);
    }

    @Override
    public DropletType getServerType(String name) {
        for (DropletType install : this.dropletTypes) {
            if (install.getName().equalsIgnoreCase(name)) {
                return install;
            }
        }
        
        return null;
    }
    
    @Override
    public void removeServerType(String name) {
        this.dropletTypes.stream().filter((installed) -> (installed.getName().equalsIgnoreCase(name))).forEachOrdered((installed) -> {
            this.dropletTypes.remove(installed);
        });
    }

    @Override
    public DOControllerImpl getControllerById(String id) {        
        return controllers.stream().filter((c) -> c.getId() != null && c.getId().equalsIgnoreCase(id)).findFirst().orElse(null);
    }
    
    @Override
    public DOControllerImpl getControllerByName(String name) {
        return controllers.stream().filter((c) -> c.getName() != null && c.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    @Override
    public Collection<DOControllerImpl> getAllControllers() {
        return Collections.unmodifiableCollection(controllers);
    }
    
    
    /**
     * Throws RuntimeException is MadsCreateEvent was cancelled externally.
     * Temporary will possibly change in the future.
     */
    @Override
    protected DOControllerImpl createController(ServerBuilder cont) {
        MadsCreateEvent event = null;
        
        if (!cont.getId().equals(DropletController.NOT_CREATED_ID)) {
            event = new MadsCreateEvent(cont.getId(), cont.getType(), true);
        } else {
            event = new MadsCreateEvent(cont.getName(), cont.getType(), false);
        }
        
        ProxyServer.getInstance().getPluginManager().callEvent(event);
        
        if (event.isCancelled()) {
            throw new RuntimeException(String.format("Droplet creation of [%s] was cancelled...", event.getName().equals(ServerController.NOT_CREATED_ID) ? event.getId() : event.getName()));
        }
        
        DropletType type = getServerType(event.getType());
        DOControllerImpl contr;
        
        if (!cont.getId().equals(ServerController.NOT_CREATED_ID)) {
            try {
                contr = new DOControllerImpl(this, backendClient, plugin, Integer.parseInt(cont.getId()));
            } catch (NumberFormatException e) {
                throw new RuntimeException("DigitalOcean ID MUST BE AN INTEGER");
            }
        } else {
            contr = new DOControllerImpl(this, backendClient, plugin, event.getName(), type);
        }
        
        contr.create();
        contr.connect();
        
        this.controllers.add(contr);
        return contr;
    }

    @Override
    protected Authentication getAuthentication() {
        //Try SecureAuth
        if (SecureAuth.exists(plugin)) {
            if (auth != null && auth instanceof SecureAuth) {
                return auth;
            }
            
            throw new RuntimeException("SecureAuth exists. Please run '/mads setup' to connect");
        }
        
        if (auth != null) {
            if (auth instanceof PlainTextAuth && ((PlainTextAuth) auth).isDefault()) {
                plugin.getLogger().warning("Using default Authentication. Please use [DFLAGS or Config]"); //or run mads setup
            }
            
            return auth;
        }
        
        for (Authentication a : Authentication.TRY_AUTHS) {
            if (a.isValid()) {
                this.auth = a;
                return a;
            }
        }
        
        this.auth = PlainTextAuth.DEFAULT;
        return getAuthentication();
    }
    
    @Override
    public void removeController(String name) {
        DropletController cont = null;
        try {
            cont = getControllerByName(name);
            cont.destroy();
        } catch (Exception e) { }
        
        if (cont != null) {
            this.controllers.remove(cont);
        }
    }
}
