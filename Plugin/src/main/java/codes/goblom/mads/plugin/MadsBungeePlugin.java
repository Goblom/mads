/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin;

import codes.goblom.mads.api.Mads;
import codes.goblom.mads.api.MadsApi;
import codes.goblom.mads.api.ServerController;
import codes.goblom.mads.api.auth.Authentication;
import codes.goblom.mads.api.utils.ThreadStarter;
import codes.goblom.mads.plugin.auth.ConfigAuth;
import codes.goblom.mads.plugin.bridge.MadsBridge;
import codes.goblom.mads.plugin.services.digitalocean.DOMadsImpl;
import codes.goblom.mads.plugin.services.local.LMadsImpl;
import codes.goblom.mads.plugin.tasks.BungeeTask;
import codes.goblom.mads.plugin.utils.Messenger;
import com.google.common.collect.Lists;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

/**
 *
 * @author Bryan Larson
 */
public class MadsBungeePlugin extends Plugin {
    @Getter //Allow support for command addons...
    private static MadsCommands madsCommands;
    
    private static final Map<String, Class<? extends MadsApi>> DEFAULT_SUPPORTED_APIS = new HashMap() {
        {
            put("DigitalOcean", DOMadsImpl.class);
            put("Local", LMadsImpl.class);
        }
    };
    
    @Getter
    private Configuration settings;
    
    @Override
    public void onLoad() {
        try {
            this.settings = getConfiguration("settings.yml");
            Authentication.TRY_AUTHS.add(new ConfigAuth(settings)); //Add config auth to TRY_AUTHS
        } catch (Exception e) {
            getLogger().info("[MADS] Unable to load settings.yml. Plugin not loading.");
            e.printStackTrace();
            return;
        }
        
        madsCommands = new MadsCommands(this);
        
        ComponentBuilder prefix = new ComponentBuilder("[");
        prefix.color(ChatColor.GRAY);
        prefix.append(settings.getBoolean("Short Prefix", false) ? "M" : "MADS");
        prefix.color(ChatColor.AQUA);
        prefix.event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, 
                    new ComponentBuilder("Minecraft Auto Deploy Systems\n\n")
                            .append("Twitter: @DevGoblom")
                            .create()));
        prefix.append("]").color(ChatColor.GRAY);
        Messenger.setPrefix(prefix);
        
        ThreadStarter.setStarter((r) -> { 
            new BungeeTask(this, r).runAsync();
        });        
        
        if (settings.getBoolean("Bridge.Enabled", false)) {
            try {
                MadsBridge.load(this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        if (MadsBridge.isEnabled()) {
            getLogger().info("Mads Bridge enabled successfully... Awaiting connections...");
        } else {
            getLogger().info("Mads Bridge is not enabled. Bungee<->MC-Server Communication disabled.");
        }
        
        String useApi = settings.getString("Server API");
        Class<? extends MadsApi> apiClass = DEFAULT_SUPPORTED_APIS.get(useApi);
        
        if (apiClass == null) {
            getLogger().warning("The use of '" + useApi + "' is not supported by default. Please have an external plugin handle the API.");
            getLogger().warning("The plugin will continue to work, but without features until another plugin sets the API instance.");
        } else {
            try {
                MadsApi madsLocal = apiClass.getConstructor(MadsBungeePlugin.class).newInstance(this);
                MadsApi.setInstance(madsLocal);
            } catch (Exception e) {
                getLogger().severe("There was an error initializing the " + useApi + " API. Mads not enabled correctly.");
                e.printStackTrace();
            }
        }
    }
    
    @Override
    public void onDisable() {
        if (Mads.getInstance() == null) return;
        
        Lists.transform(new ArrayList<>(Mads.getAllControllers()), ServerController::getName).forEach(Mads::removeController);
        MadsApi.setInstance(null);
    }
    
    public Configuration getDefaultConfiguration() {
        return this.settings;
    }
    
    public Configuration getConfiguration(String name) throws IOException {
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }

        File configFile = new File(getDataFolder(), name);
        if (!configFile.exists()) {
            try (InputStream in = getResourceAsStream(name)) {
                Files.copy(in, configFile.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
    }
    
    //TODO: Update this to be better
    protected void updateAuth(Authentication auth) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
        if (MadsApi.getInstance() == null) {
            throw new NullPointerException("MadsApi is not setup correctly.");
        }
        
        boolean updated = false;
        
        for (Field field : MadsApi.getInstance().getClass().getDeclaredFields()) {
            if (field.getType().equals(Authentication.class)) {
                field.setAccessible(true);
                field.set(MadsApi.getInstance(), auth);
                updated = true;
                break;
            }
        }
        
        if (!updated) {
            Field f = MadsApi.getInstance().getClass().getField("auth");
            f.setAccessible(true);
            f.set(MadsApi.getInstance(), auth);
        }
    }
}
