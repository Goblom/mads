/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api;

import codes.goblom.mads.api.ssh.SSHController;
import java.util.List;
import lombok.Getter;

/**
 *
 * @author Bryan Larson
 */
public abstract class ServerController<A extends MadsApi> {
    public static final String NOT_CREATED_ID = "MADS_NOT_CREATED";
    
    protected final A api;
    
    @Getter
    protected String name;
    
    @Getter
    protected String id;
    
    @Getter
    protected ServerType type;
    
    public ServerController(A api, String id) {
        this.api = api;
        this.id = id;
    }
    
    public ServerController(A api, String name, ServerType type) {
        this.api = api;
        this.type = type;
        this.name = name;
    }
    
    public abstract void create();
    
    public abstract void connect();
    
    public abstract boolean isConnected();
    
    public abstract boolean isOnline();
    
    public abstract boolean exists();
    
    public abstract boolean destroy();
    
    public abstract List<String> getIpAddresses();
    
    public abstract SSHController getSSHController();
}
