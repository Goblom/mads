/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

/**
 *
 * @author Bryan Larson
 */
@Getter
@Setter
public final class ServerBuilder {
    
    String name;
    String type;
    String id = ServerController.NOT_CREATED_ID;
    
    private final MadsApi api;
    
    protected ServerBuilder(@NonNull MadsApi api) {
        this.api = api;
    }
    
    public ServerBuilder id(String id) {
        this.id = id;
        
        return this;
    }
    
    public ServerBuilder name(String name) {
        this.name = name;
        
        return this;
    }
    
    public ServerBuilder type(String type) {
        this.type = type;
        
        return this;
    }
    
    /**
     * Note by build() you are creating the instance 
     */
    public ServerController build() {
        if (!id.equals(ServerController.NOT_CREATED_ID)) {
            if (api.getControllerByName(id) != null) {
                throw new UnsupportedOperationException("Cannot have multiple server types with the same id.");
            }
        }
        
        if (api.getControllerByName(name) != null) {
            throw new UnsupportedOperationException("Cannot have multiple server types with the same name.");
        }
        
        return api.createController(this);
    }
}
