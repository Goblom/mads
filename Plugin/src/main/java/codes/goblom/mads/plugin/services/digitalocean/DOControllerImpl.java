/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin.services.digitalocean;

import codes.goblom.mads.api.auth.Authentication;
import codes.goblom.mads.plugin.Permissions;
import codes.goblom.mads.api.services.digitalocean.DropletController;
import codes.goblom.mads.api.events.MadsConnectEvent;
import codes.goblom.mads.api.services.digitalocean.DropletType;
import codes.goblom.mads.api.ssh.impl.jsch.JSchSSHController;
import codes.goblom.mads.api.ssh.impl.jsch.JSchProgressMonitor;
import codes.goblom.mads.plugin.MadsBungeePlugin;
import codes.goblom.mads.plugin.callbacks.SenderUploadProgressMonitor;
import codes.goblom.mads.plugin.tasks.BungeeTask;
import codes.goblom.mads.plugin.tasks.OnlinePinger;
import codes.goblom.mads.plugin.utils.Messenger;
import com.google.common.collect.Lists;
import com.myjeeva.digitalocean.DigitalOcean;
import com.myjeeva.digitalocean.pojo.Droplet;
import com.myjeeva.digitalocean.pojo.Network;
import java.io.File;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 *
 * @author Bryan Larson
 */
public class DOControllerImpl extends DropletController<DOMadsImpl> {

    private final MadsBungeePlugin plugin;
    private JSchSSHController sshController;

    public DOControllerImpl(DOMadsImpl api, DigitalOcean client, MadsBungeePlugin plugin, int id) {
        super(api, client, id);
        
        this.plugin = plugin;
    }
    
    public DOControllerImpl(DOMadsImpl api, DigitalOcean client, MadsBungeePlugin plugin, String name, DropletType type) {
        super(api, client, name, type);
        
        this.plugin = plugin;
    }
    
    @Override
    public void connect() {
        if (isConnected()) return;
        sshController = null; //Force it to be null so we can set it.
        
        new BungeeTask(plugin) {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(TimeUnit.SECONDS.toMillis(5));
                    } catch (Exception e) { }

                    Droplet droplet = getDroplet();

                    if (droplet.isNew()) continue;
                    if (!droplet.isActive()) continue;
                    break;
                }

                try {
                    Thread.sleep(TimeUnit.SECONDS.toMillis(30)); //Todo: Make configurable.
                } catch (Exception e) { e.printStackTrace(); }

                getSSHController().connect();
                onConnect();
            }
        }.runAsync();
    }
    
    @Override
    public boolean isConnected() {
        if (sshController == null) return false;
        
        return sshController.isConnected();
    }
    
    @Override
    public JSchSSHController getSSHController() {
        if (sshController == null) {
            sshController = new JSchSSHController(this, ((DOMadsImpl) api).getAuthentication());
        }
        
        return sshController;
    }
    
    @Override
    public List<String> getIpAddresses()  { 
        try {
            List<Network> networks = getDroplet().getNetworks().getVersion4Networks();
            return Lists.transform(networks, Network::getIpAddress);
        } catch (Exception e) { }
        
        return new ArrayList();
    }
    
    @Override
    public void onCreate() {
        for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
            if (Permissions.NOTIFY.has(player) || Permissions.NOTIFY_CREATE.has(player)) {
                Messenger.sendMessage(player, "Droplet " + getName() + " was created.");
            }
        }
    }

    @Override
    public void onConnect() {
        for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
            if (Permissions.NOTIFY.has(player) || Permissions.NOTIFY_CONNECT.has(player)) {
                Messenger.sendMessage(player, "Droplet " + getName() + " was successfully connected.");
            }
        }
        
        String ip = getIpAddresses().get(0);
        ServerInfo info = ProxyServer.getInstance().constructServerInfo(getName(), new InetSocketAddress(ip, 25565), "", false);
        
        new OnlinePinger(plugin, info) {
            @Override
            public boolean onPing(ServerPing ping, Throwable error) {                
                if (isCancelled() || error != null) return false;
                
                ComponentBuilder join = new ComponentBuilder("/server " + info.getName())
                        .color(ChatColor.GOLD)
                        .event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/server " + info.getName()))
                        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText("Click to execute this command")));

                String m1 = "BungeeCords says " + getName() + " is online. Try it out!";
                
                for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
                    if (Permissions.NOTIFY.has(player) || Permissions.NOTIFY_ONLINE.has(player)) {
                        Messenger.sendMessage(player, m1);
                        Messenger.sendMessage(player, join);
                    }
                }
                
                plugin.getProxy().getServers().put(info.getName(), info);
                
                return true;
            }
        }.schedule(30, 30, TimeUnit.SECONDS);
        
        List<String> initCmds = getType().getInitCmds();
        
        if (initCmds != null && !initCmds.isEmpty()) {
            for (String exec : initCmds) {
                if (exec.startsWith("@") || exec.startsWith("$")) {
                    exec = exec.substring(1);
                    String[] args = exec.split(" ");
                    
                    switch (args[0].toLowerCase()) {
                        case "upload":
                        case "uploadfile":
                            String fileStr = null;
                            String output = null;
                            boolean unzip = false;
                            
                            try {
                                fileStr = args[1];
                            } catch (Exception e) {
                            }
                            try {
                                unzip = args[2].equalsIgnoreCase("-unzip");
                                if (!unzip) {
                                    output = args[2];
                                }
                            } catch (Exception e) { }
                            try {
                                unzip = args[3].equalsIgnoreCase("-unzip");
                            } catch (Exception e) { }
                            
                            if (fileStr == null || fileStr.isEmpty()) {
                                plugin.getLogger().log(Level.WARNING, "uploadFile requires a file... try @uploadFile [file]");
                                plugin.getLogger().log(Level.WARNING, "Skipping uploadFile...");
                                break;
                            }
                            File file = new File(plugin.getDataFolder(), fileStr);

                            if (!file.exists()) {
                                plugin.getLogger().log(Level.WARNING, "uploadFile [{0}] does not exist. Skipping...", fileStr);
                                break;
                            }

                            if (file.isDirectory()) {
                                plugin.getLogger().log(Level.WARNING, "uploadFile [{0}] does not support directories. Skipping...", fileStr);
                                break;
                            }
                            
                            final boolean toUnzip = unzip;
                            JSchProgressMonitor upm = new SenderUploadProgressMonitor(ProxyServer.getInstance().getConsole()) {
                                @Override
                                public void accept(UploadStatus t) {
                                    super.accept(t);
                                    if (t == UploadStatus.FINISHED && toUnzip) {
//                                        List<String> exec = getSSHController().getOutputOf("unzip " + getFile().getName()); //May not work if output is not null
//                                        plugin.getLogger().info("Unzip Output: " + exec);
                                        getSSHController().exec("unzip " + getFile().getName());
                                    }
                                }
                            };
                            if (output != null) {
                                getSSHController().upload(file, output, upm);
                            } else {
                                getSSHController().upload(file, upm);
                            }
                            break;
                        case "uploadfolder":
                        case "uploaddir":
                            plugin.getLogger().log(Level.WARNING, "Notice: uploadDir not supported yet.");
                            break;
                    }
                } else {
                    int wait = 0;
                    try {
                        if (exec.startsWith("w:") || exec.startsWith("wait:")) {
                            String[] strs = exec.split(" ");
//                            String waitstr = strs[0].split(":")[1];
                            wait = Integer.parseInt(strs[0].split(":")[1]);
                            
//                            plugin.getLogger().info("Waiting " + wait + " seconds to run next command");
//                            Thread.sleep(TimeUnit.SECONDS.toMillis(Integer.parseInt(waitstr)));
                            
                            exec = exec.replace(strs[0] + " ", "");
                        }
                    } catch (NumberFormatException e) { 
                        throw new RuntimeException(e);
                    }
                    
                    if (wait > 0) {
                        final String finalExec = exec;
                        ProxyServer.getInstance().getScheduler().schedule(plugin, () -> {
                            getSSHController().exec(finalExec); 
                        }, wait, TimeUnit.SECONDS);
                    } else {
                        getSSHController().exec(exec);
                    }
//                    List<String> output = getSSHController().exec(exec);
//                    System.out.println("exec = '" + exec + "'");
//                    System.out.println("output = " + output);
                }
            }
        }
        
        ProxyServer.getInstance().getPluginManager().callEvent(new MadsConnectEvent(this));
    }

    @Override
    protected Authentication getAuthentication() {
        return api.getAuthentication();
    }

    @Override
    public boolean isOnline() {
        return this.isActive();
    }
}
