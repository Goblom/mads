/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin.exec;

import codes.goblom.mads.api.Mads;
import codes.goblom.mads.plugin.utils.Messenger;
import com.google.common.collect.Lists;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import lombok.Setter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.TabExecutor;

/**
 *
 * @author Bryan Larson
 */
public abstract class Executor extends Command implements TabExecutor {

    private final List<ExecMap> execMap = new ArrayList();
    
    @Setter
    private ComponentBuilder noPermissionMessage;
    
    private final Plugin plugin;
    
    public Executor(Plugin plugin, String name) {
        super(name);
        this.plugin = plugin;
    }

    public final List<CommandInfo> getRegisteredCommands() {
        return Lists.transform(execMap, ExecMap::getInfo);
    }
    
    public final void addExecutor(CommandListener exec) {
        Class<?> clazz = exec.getClass();
        
        while (clazz != null) {
            for (Method m : clazz.getDeclaredMethods()) {
                if (m.isAnnotationPresent(CommandInfo.class)) {
                    CommandInfo info = m.getAnnotation(CommandInfo.class);
                    m.setAccessible(true);
                    execMap.add(new ExecMap(m, info, exec));
                    
//                    plugin.getLogger().info("Executor found method[" + m.getName() + "] with command of [" + info.name() + "] in [" + clazz.toString() + "]");
                }
            }
            
            clazz = clazz.getSuperclass();
        }
    }
    
    @Override
    public final void execute(CommandSender sender, String[] args) {
        if (Mads.getInstance() == null) {
            Messenger.sendMessage(sender, ChatColor.RED + "MadsAPI not loaded; Command not executing. Please check console");
            return;
        }
        
        if (args.length == 0) {
            sendHelp(new CommandContext(this, sender, args));
            return;
        }
        
        String cmd = null;
        try {
            cmd = ChatColor.stripColor(args[0]);
        } catch (Exception e) {
            sendHelp(new CommandContext(this, sender, args));
            return;
        }
        
        if (cmd == null || cmd.isEmpty()) {
            sendHelp(new CommandContext(this, sender, args));
            return;
        }
        
        ExecMap found = null;
        
        for (ExecMap map : execMap) {
            CommandInfo info = map.getInfo();
            
            if (info.name().equalsIgnoreCase(cmd)) {
                found = map;
                break;
            } else {
                if (info.alias().length != 0) {
                    for (String alias : info.alias()) {
                        if (alias.equalsIgnoreCase(cmd)) {
                            found = map;
                            break;
                        }
                    }
                }
            }
        }
        
        if (found == null) {
            sendHelp(new CommandContext(this, sender, args));
            return;
        }
        
        if (!found.getInfo().permission().has(sender)) {
            if (noPermissionMessage != null) {
                Messenger.sendMessage(sender, noPermissionMessage);
            }
            return;
        }
        
        final CommandContext context = new CommandContext(this, sender, args, true);
        final ExecMap ffound = found;
        
        Runnable r = () -> {
            try {
                ffound.getMethod().invoke(ffound.getListener(), context);
            } catch (Throwable e) { //Use generic Throwable because a command may throw a different error
                onError(sender, e);
            }
        };
        
        if (found.getInfo().async()) {
            ProxyServer.getInstance().getScheduler().runAsync(plugin, r);
        } else {
            r.run();
        }
    }

    @Override
    public final Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if (Mads.getInstance() == null) {
            return null;
        }
        
        List<String> suggest = new ArrayList();
        
        ExecMap found = null;
        
        try {
            String cmd = ChatColor.stripColor(args[0]);
            
            for (ExecMap map : execMap) {
                CommandInfo info = map.getInfo();

                if (info.name().equalsIgnoreCase(cmd)) {
                    found = map;
                    break;
                } else {
                    if (info.alias().length != 0) {
                        for (String alias : info.alias()) {
                            if (alias.equalsIgnoreCase(cmd)) {
                                found = map;
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) { }
        
        if (found == null) {
            if (args.length > 1) {
                suggest.add(ChatColor.BOLD + "" + ChatColor.RED + "Command " + args[0] + " not found.");
                if (sender instanceof ProxiedPlayer) {
                    return suggest;
                }
            
                return Lists.transform(suggest, ChatColor::stripColor);
            }
            
            List<String> available = new ArrayList();
            List<String> addon = new ArrayList();
            
            execMap.stream().filter((map) -> !(!map.getInfo().permission().has(sender))).forEachOrdered((map) -> {
                String name = map.getInfo().name();
                try {
                    String cmd = args[0];
                    
                    if (name.startsWith(cmd) || name.endsWith(cmd)) {
                        if (map.getListener().getOwningPlugin().equals(this.plugin)) {
                            if (available.isEmpty()) {
                                available.add(ChatColor.GREEN + "" + ChatColor.BOLD + ChatColor.ITALIC + "" + ChatColor.UNDERLINE + "Available Commands: ");
                            }
                            StringBuilder desc = new StringBuilder().append(ChatColor.GOLD).append(ChatColor.BOLD).append(name);
                            if (map.getInfo().description() != null && !map.getInfo().description().isEmpty()) {
                                desc.append(ChatColor.RESET);
                                desc.append(ChatColor.GRAY).append(" - ").append(ChatColor.BLUE).append(ChatColor.ITALIC).append(map.getInfo().description());
                            }
                            
                            available.add(desc.toString());
                        } else {
                            if (available.isEmpty()) {
                                available.add(ChatColor.GREEN + "" + ChatColor.BOLD + ChatColor.ITALIC + "" + ChatColor.UNDERLINE + "Addon Commands:");
                            }
                            StringBuilder desc = new StringBuilder().append(ChatColor.GOLD).append(ChatColor.BOLD).append(name);
                            if (map.getInfo().description() != null && !map.getInfo().description().isEmpty()) {
                                desc.append(ChatColor.RESET);
                                desc.append(ChatColor.GRAY).append(" - ").append(ChatColor.BLUE).append(ChatColor.ITALIC).append(map.getInfo().description());
                            }
                            
                            available.add(desc.toString());
                        }
                    }
                } catch (Exception e) { }
            });
            
            if (!addon.isEmpty() && !available.isEmpty()) {
                int spacing = 2;
                for (int i = 0; i < spacing; i++) {
                    available.add("");
                }
            }
            
            if (!available.isEmpty()) suggest.addAll(available);
            if (!addon.isEmpty()) suggest.addAll(addon);
            
            if (sender instanceof ProxiedPlayer) {
                return suggest;
            }
            
            return Lists.transform(suggest, ChatColor::stripColor);
        }
        
        if (!found.getInfo().permission().has(sender)) {
            if (noPermissionMessage != null) {
                suggest.add(TextComponent.toPlainText(noPermissionMessage.create()));
            }
            
            if (sender instanceof ProxiedPlayer) {
                return suggest;
            }
            
            return Lists.transform(suggest, ChatColor::stripColor);
        }
        
        final CommandContext context = new CommandContext(this, sender, args, false);
        final ExecMap ffound = found;
        
        try {
            ffound.getMethod().invoke(ffound.getListener(), context);
        } catch (Exception e) { //Use generic Exception because a command may throw a different error
            onError(sender, e);
        }
        
        if (context.getTabComplete() != null && !context.getTabComplete().isEmpty()) {
            return context.getTabComplete();
        }
        
//        suggest.add("context.getTabComplete() is null or empty");
        return suggest;
    }
    
    /**
     * Triggered only if unable to find a command
     */
    public abstract void sendHelp(CommandContext context);
    
    /**
     * This is only called if there is an error when running a @CommandInfo command.
     * Not triggered anywhere else.
     * 
     * @param sender CommandSender who ran command
     * @param e Exception thrown
     */
    public abstract void onError(CommandSender sender, Throwable e);
    
    public static List<String> copyPartialMatches(List<String> orig, String token) {
        List<String> coll = new ArrayList();
        
        for (String str : orig) {            
            if (token != null && (str.length() < token.length())) continue;
            
            if (str.regionMatches(true, 0, token, 0, token.length())) {
                coll.add(str);
            }
        }
        
        return coll;
    }
}
