/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api;

import java.util.Collection;
import java.util.List;

/**
 *
 * @author Bryan Larson
 */
public class Mads {
    
    public static MadsApi getInstance() {
        return MadsApi.getInstance();
    }
    
    public static void reload() {
        MadsApi.getInstance().reload();
    }
    
    public static boolean addServerType(ServerType type) {
        return MadsApi.getInstance().addServerType(type);
    }
    
    public static List<ServerType> getServerTypes() {
        return MadsApi.getInstance().getServerTypes();
    }
    
    public static ServerType getServerType(String name) {
        return MadsApi.getInstance().getServerType(name);
    }
    
    public static void removeServerType(String name) {
        MadsApi.getInstance().removeServerType(name);
    }
    
    public static ServerController getControllerById(String id) {
        return MadsApi.getInstance().getControllerByName(id);
    }
    
    public static ServerController getControllerByName(String name) {
        return MadsApi.getInstance().getControllerByName(name);
    }
    
    public static void removeController(String name) {
        MadsApi.getInstance().removeController(name);
    }
    
    public static Collection<ServerController> getAllControllers() {
        return MadsApi.getInstance().getAllControllers();
    }
    
    public static ServerBuilder buildController() {
        return MadsApi.getInstance().buildController();
    }
}
