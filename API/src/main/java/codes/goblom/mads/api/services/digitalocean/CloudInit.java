/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.services.digitalocean;

import codes.goblom.mads.api.auth.Authentication;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 *
 * @author Bryan Larson
 */
@Data
public abstract class CloudInit {

    List<User> users;
    List<Group> groups;
    ChangePassword chpasswd;
    boolean package_upgrade;
    boolean package_update;
    List<String> packages;
    List<String> runcmd;

    public void load() { }

    public void save() { }

    public abstract String toString(Authentication auth);

    @NoArgsConstructor
    @Data
    public static class User {

        String name;
//        String primary_group;
//        List<String> groups;
        String shell = "/bin/bash";
        String sudo;
//        List<String> ssh_authorized_keys;
        String passwd; //Hashed Password
//        String gecos;
//        String expiredate;
//        boolean lock_password;
//        boolean inactive;
//        boolean system;
//        String homedir;
//        String selinux_user;
//        boolean no_create_home;
//        boolean no_user_group;
//        boolean no_log_init;
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public static class Group {

        String name;
        List<String> defaultUsers;
    }

    @Data
    public static class ChangePassword {

        Map<String, String> list;
        boolean expire = false;

        public void add(@NonNull String username, @NonNull String plainPassword) {
            if (list == null) {
                this.list = new HashMap();
            }

            if (plainPassword.equalsIgnoreCase("RANDOM") || plainPassword.equalsIgnoreCase("R")) {
                throw new UnsupportedOperationException("CHPASSWORD does not currently support 'RANDOM' or 'R'");
            }

            list.put(username, plainPassword);
        }
    }
    
    /**
     * 
     * @deprecated not used
     */
    @Deprecated
    @Data
    public static class PowerState {

        int timeout;
        int delate;
        String message;
        PowerMode mode;

        public static enum PowerMode {
            POWEROFF, REBOOT, HALT;
        }
    }
}
