/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin.tasks;

import codes.goblom.mads.plugin.MadsBungeePlugin;
import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.config.ServerInfo;

/**
 *
 * @author Bryan Larson
 */
public abstract class OnlinePinger extends BungeeTask {
    
    private final ServerInfo serverInfo;
    
    public OnlinePinger(MadsBungeePlugin plugin, ServerInfo info) {
        super(plugin);
        
        this.serverInfo = info;
    }
    
    @Override
    public final void run() {
        if (isCancelled()) return;
        
        Callback<ServerPing> call = (ping, err) -> {
            if (onPing(ping, err)) {
                setCancelled(true);
            }
        };
        
        serverInfo.ping(call);
    }
    
    public abstract boolean onPing(ServerPing ping, Throwable error);
}
