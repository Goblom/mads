/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin.auth;

import codes.goblom.mads.api.auth.Authentication;
import codes.goblom.mads.api.auth.PlainTextAuth;
import lombok.RequiredArgsConstructor;
import net.md_5.bungee.config.Configuration;

/**
 *
 * @author Bryan Larson
 */
@RequiredArgsConstructor
public class ConfigAuth implements Authentication {

    private static final String USERNAME_PATH = "Authentication.Username";
    private static final String PASSWORD_PATH = "Authentication.Password";
    
    private final Configuration config;
    
    @Override
    public boolean isValid() {
        return config.contains(USERNAME_PATH) && config.contains(PASSWORD_PATH);
    }
    
    @Override
    public String getUsername() {
        return config.getString("Authentication.Username", PlainTextAuth.DEFAULT_USERNAME);
    }

    @Override
    public String getPassword() {
        return config.getString("Authentication.Password", PlainTextAuth.DEFAULT_PASSWORD);
    }
    
}
