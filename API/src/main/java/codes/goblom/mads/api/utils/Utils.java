/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Random;

/**
 *
 * @author Bryan Larson
 */
public class Utils {
    private static final Random RANDOM = new Random();
    
    public static int findRandomPort(int start, int end) {
        int difference = end - start;
        int trys = 0;
        int temp = 0;
        
        while (trys < difference) {
            temp = RANDOM.nextInt(difference) + start;
            
            try {
                new ServerSocket(temp).close();
                break;
            } catch (Exception e) { 
                trys++;
            }
        }
        
        return temp;
    }

    public static void copyFilesInDirectory(File fromDir, File toDir) throws IOException {
        if (!fromDir.isDirectory()) return;
        if (!toDir.exists()) toDir.mkdirs();
        
        File[] files = fromDir.listFiles();
        
        for (File file : files) {
            File path = new File(toDir, file.getName());
            
            if (file.isDirectory()) {
                copyFilesInDirectory(file, path);
            } else {
                Files.copy(file.toPath(), path.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }
        }
    }
    
    public static void copyFile(File from, File toFolder) throws IOException {
//        System.out.println("From[" + from + "] to[" + toFolder + "]");
        
        if (from.isDirectory()) {
            copyFilesInDirectory(from, toFolder);
        } else {
            Files.copy(from.toPath(), toFolder.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
    }
    
    public static boolean delete(File file) {
        if (!file.delete()) {
            for (File f : file.listFiles()) {
                delete(f);
            }
        }
        
        return file.delete();
    }
    
        /**
     * https://stackoverflow.com/a/14541376
     */
    public static String getIpAddress() {
        BufferedReader in = null;
        try {
            URL whatismyip = new URL("http://checkip.amazonaws.com");
            in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
            return in.readLine();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return null;
    }
}
