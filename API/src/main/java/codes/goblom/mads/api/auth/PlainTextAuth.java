/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.auth;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 *
 * @author Bryan Larson
 */
@RequiredArgsConstructor
@Getter
public class PlainTextAuth implements Authentication {
    public static final String DEFAULT_USERNAME = "mads";
    public static final String DEFAULT_PASSWORD = "MADS635241";
    
    public static final Authentication DEFAULT = new PlainTextAuth(DEFAULT_USERNAME, DEFAULT_PASSWORD) {
        @Override
        public boolean isDefault() {
            return true;
        }
    };
    
    private final String username;
    private final String password;
    
    public boolean isDefault() {
        return false;
    }
}
