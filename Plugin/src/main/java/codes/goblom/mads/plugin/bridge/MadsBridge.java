/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin.bridge;

import codes.goblom.mads.api.ServerController;
import codes.goblom.mads.api.sockets.SocketConnection;
import codes.goblom.mads.api.sockets.SocketServer;
import codes.goblom.mads.api.events.MadsConnectEvent;
import codes.goblom.mads.api.events.bridge.proxy.BridgeConnectEvent;
import codes.goblom.mads.api.events.bridge.proxy.BridgeReceiveEvent;
import codes.goblom.mads.api.utils.ThreadStarter;
import codes.goblom.mads.plugin.MadsBungeePlugin;
import codes.goblom.mads.plugin.callbacks.SenderUploadProgressMonitor;
import codes.goblom.mads.plugin.tasks.OnlinePinger;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import codes.goblom.mads.plugin.tasks.BungeeTask;
import codes.goblom.mads.api.sockets.SocketReceiver;
import codes.goblom.mads.api.utils.Utils;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Bryan Larson
 */
public class MadsBridge implements Listener, SocketReceiver {
    
    private static boolean enabled = false;
    private static Map<String, SocketConnection> CONNECTIONS = new HashMap();
    
    public static boolean isEnabled() {
        return enabled;
    }
    
    public static void load(MadsBungeePlugin plugin) throws IOException {
        if (isEnabled()) return;
        
        MadsBridge bridge = new MadsBridge(plugin);
        
        MadsBridge.enabled = true;
    }
    
    private final MadsBungeePlugin plugin;
    private final int socketPort;
    private final SocketServer fServer;
    
    private MadsBridge(MadsBungeePlugin plugin) throws IOException {
        this.plugin = plugin;
        this.socketPort = plugin.getDefaultConfiguration().getInt("Bridge.Port", BridgeInfo.DEFAULT_PORT);
        this.fServer = new SocketServer(socketPort) {
            @Override
            public void onConnect(SocketConnection conn) {
                onConnect0(conn);
            }
        };
        this.fServer.setReciever(this);
        ThreadStarter.start(fServer);
        
        ProxyServer.getInstance().registerChannel(BridgeInfo.CHANNEL);
        ProxyServer.getInstance().getPluginManager().registerListener(plugin, this);
    }
    
    @EventHandler
    public void onPluginMessage(PluginMessageEvent event) {
        if (event.getTag().equalsIgnoreCase(BridgeInfo.CHANNEL)) {
            plugin.getLogger().info("Recieved tag with length of... " + event.getData().length);
        }
    }
    
    @EventHandler
    public void onMadsConnect(MadsConnectEvent event) {        
        final int port = socketPort;
        final String ip = Utils.getIpAddress();
        if (ip == null) return;
        
        ServerController cont = event.getController();
        plugin.getLogger().info("[Bridge] Detected a Connect Event... Uploading Bridge...");
        
        cont.getSSHController().getSftp().upload(plugin.getFile(), "plugins", new SenderUploadProgressMonitor(ProxyServer.getInstance().getConsole()));
        cont.getSSHController().getExec().exec("echo -e '" + BridgeInfo.BRIDGE_PLUGIN_CONFIG.replace("{ip}", ip).replace("{port}", "" + port) + "' > plugins/Mads-Bridge/bridge.cfg");
        
        
        new BungeeTask(plugin) {
            @Override
            public void run() {
                final ServerInfo info = ProxyServer.getInstance().getServerInfo(cont.getName());
                
                if (info != null) {
                    setCancelled(true);
                    
                    new OnlinePinger(plugin, info) {
                        @Override
                        public boolean onPing(ServerPing ping, Throwable error) {
                            if (ping != null) {
                                try {
                                    send(info, "port-" + port +",host-" + ip);
                                    return true;
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                            return false;
                        }
                    }.schedule(10, 3, TimeUnit.SECONDS);
                }
            }
        }.schedule(1, 5, TimeUnit.SECONDS);
    }
    
    protected void send(ServerInfo info, String... strs) throws IOException {
        send(info, false, strs);
    }
    
    protected void send(ServerInfo info, boolean queue, String... strs) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);

        for (String str : strs) {
            if (str == null) continue;
            
            out.writeUTF(str);
        }
        
        final byte[] sending = stream.toByteArray();
        
        if (!queue) {
            info.sendData(BridgeInfo.CHANNEL, sending);
        } else {
            info.sendData(BridgeInfo.CHANNEL, sending, true);
        }
    }

    private void onConnect0(SocketConnection connection) {
        plugin.getLogger().info("[Bridge] Successfully connected to.... " + connection.getAddress().getHostAddress());
        
        ServerInfo info = tryServerInfo(connection);
        if (info == null) return;
        CONNECTIONS.put(info.getName(), connection);
        ProxyServer.getInstance().getPluginManager().callEvent(new BridgeConnectEvent(info, connection));
    }
    
    @Override
    public void onReceive(SocketConnection connection, Object received) {
        plugin.getLogger().info("[Bridge] Recieved object from... " + connection.getAddress());
        
        ServerInfo info = tryServerInfo(connection);
        ProxyServer.getInstance().getPluginManager().callEvent(new BridgeReceiveEvent(info, connection, received));
    }
    
    private static final ServerInfo tryServerInfo(SocketConnection connection) {
        String remoteIp = connection.getAddress().getHostAddress();
        for (ServerInfo info : ProxyServer.getInstance().getServers().values()) {
            if (info.getAddress().getAddress().getHostAddress().equals(remoteIp)) {
                return info;
            }
        }
        
        return null;
    }
    
    public static SocketConnection getConnection(String name) {
        if (CONNECTIONS.containsKey(name)) {
            return CONNECTIONS.get(name);
        }
        
        return null;
    }
}
