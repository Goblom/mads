/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.auth;

import lombok.Getter;

/**
 *
 * @author Bryan Larson
 */
public class DflagAuth implements Authentication {

    public static final String FLAG_USERNAME = "Mads.Username";
    public static final String FLAG_PASSWORD = "Mads.Password";
//    public static final String FLAG_SSH_KEY = "Mads.SSHKey";
    
//    private final boolean isSSHKey;
    @Getter
    private final String username;
    private final String key;
    
    public DflagAuth() {
        String objUser = System.getProperty(FLAG_USERNAME);
        if (objUser == null || objUser.isEmpty()) {
            objUser = PlainTextAuth.DEFAULT_USERNAME; //Default
        }
        
        this.username = objUser;
        
        String objPass = System.getProperty(FLAG_PASSWORD);
//        if (objPass == null || objPass.isEmpty()) {
//            this.key = System.getProperty(FLAG_SSH_KEY);
//            this.isSSHKey = true;
//        } else {
            this.key = objPass;
//            this.isSSHKey = false;
//        }
    }
    
    @Override
    public String getPassword() {
        return this.key;
    }
//    
//    public boolean isSSHKey() {
//        return isSSHKey;
//    }
//    
    @Override
    public boolean isValid() {
        return 
                (username != null && !username.isEmpty()) &&
                (key != null && !key.isEmpty());
    }
}
