/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import net.md_5.bungee.api.ProxyServer;

/**
 *
 * @author Bryan Larson
 */
public class Crypt {
    
//    public static void main(String[] args) throws Exception {
//        SerialAuth auth = new SecureAuth("Goblom", "fa72b5a8ac714a4e");
//        String password = "my_16_char_passs";
//        encrypt(auth, password, "target/auth.aes");
//        
//        Authentication dec = decrypt("target/auth.aes", password);
//        
//        System.out.println("username = " + dec.getUsername());
//        System.out.println("password = " + dec.getPassword());
//    }
 
    private static final String UID = ProxyServer.getInstance().getConfig().getUuid().replace("-", "");
    protected static byte[] salt() {
        return UID.substring(0, UID.length() - (UID.length() - 8)).getBytes();
    }
    
    private static String make16(String str) {
        if (str.length() == 16) {
            return str;
        } else if (str.length() > 16) {
            str = str.substring(0, str.length() - (str.length() - 16));
        }
        
        int i = 0;
        while (str.length() != 16) {
            str = str + UID.toCharArray()[i++];
        }
        
        return str;
    }
    
    public static Object encrypt(Serializable o, File file, String password) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IOException, IllegalBlockSizeException, InvalidKeySpecException {
//        if (password.length() != 16) {
//            throw new InvalidAlgorithmParameterException("Password MUST be 16 bytes in length");
//        }
        password = make16(password);
        
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt(), 10000, 128);
        SecretKey tmp = factory.generateSecret(spec);
        SecretKeySpec KEY = new SecretKeySpec(tmp.getEncoded(), "AES");
        
        byte[] iv = password.getBytes();
        
        Cipher cipher = Cipher.getInstance(KEY.getAlgorithm() + "/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, KEY, new IvParameterSpec(iv));
        
        SealedObject sealed = new SealedObject(o, cipher);
        
        if (file != null) {
            FileOutputStream fos = new FileOutputStream(file);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            CipherOutputStream cos = new CipherOutputStream(bos, cipher);
            ObjectOutputStream oos = new ObjectOutputStream(cos);

            oos.writeObject(sealed);
            oos.close();
        }
        return sealed;
    }
    
    public static <O extends Serializable> O decrypt(File file, String password) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, FileNotFoundException, IOException, ClassNotFoundException, IllegalBlockSizeException, BadPaddingException {
//        if (password.length() != 16) {
//            throw new InvalidAlgorithmParameterException("Password MUST be 16 bytes in length");
//        }
        password = make16(password);
        
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt(), 10000, 128);
        SecretKey tmp = factory.generateSecret(spec);
        SecretKeySpec KEY = new SecretKeySpec(tmp.getEncoded(), "AES");
        
        byte[] iv = password.getBytes();
        
        Cipher cipher = Cipher.getInstance(KEY.getAlgorithm() + "/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, KEY, new IvParameterSpec(iv));
        
        CipherInputStream cis = new CipherInputStream(new BufferedInputStream(new FileInputStream(file)), cipher);
        ObjectInputStream ois = new ObjectInputStream(cis);
        SealedObject sealed = (SealedObject) ois.readObject();
        
        return (O) sealed.getObject(cipher);
    }
}
