/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.ssh.wrappers;

import codes.goblom.mads.api.ssh.UploadProgressMonitor;
import codes.goblom.mads.api.ssh.Wrapped;
import java.io.File;

/**
 *
 * @author Bryan Larson
 */
public interface WrappedSftp extends Wrapped {
    
    public void upload(File file);
    
    public void upload(File file, String dir);
        
    public void upload(File file, UploadProgressMonitor monitor);
    
    public void upload(File file, String dir, UploadProgressMonitor monitor);
}
