/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.services.digitalocean;

import codes.goblom.mads.api.MadsApi;
import codes.goblom.mads.api.ServerController;
import codes.goblom.mads.api.auth.Authentication;
import codes.goblom.mads.api.auth.DflagAuth;
import com.myjeeva.digitalocean.DigitalOcean;
import com.myjeeva.digitalocean.exception.DigitalOceanException;
import com.myjeeva.digitalocean.exception.RequestUnsuccessfulException;
import com.myjeeva.digitalocean.pojo.Droplet;
import com.myjeeva.digitalocean.pojo.Droplets;
import com.myjeeva.digitalocean.pojo.Image;
import com.myjeeva.digitalocean.pojo.Region;
import java.util.Objects;

/**
 *
 * @author Bryan Larson
 */
public abstract class DropletController<A extends MadsApi> extends ServerController<A> {
    
    protected final DigitalOcean client;
    
    /**
     * This assumes that the Droplet exists in DigitalOcean
     */
    public DropletController(A api, DigitalOcean client, int id) {
        super(api, String.valueOf(id));
        
        this.type = null;
        this.client = client;
        
        try {
            this.name = client.getDropletInfo(id).getName();
        } catch (DigitalOceanException | RequestUnsuccessfulException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * This assumes that the Droplet does not necessarily exists in DigitalOcean
     */
    public DropletController(A api, DigitalOcean client, String dropletName, DropletType type) {
        super(api, dropletName, type);
        
        this.client = client;
        
        //Hackey - To Determine if droplet name is already associated with DigitalOcean
        try {
            Droplets droplets = client.getAvailableDroplets(1, 0);

            for (Droplet d : droplets.getDroplets()) {
                if (d.getName().equalsIgnoreCase(name)) {
                    this.id = String.valueOf(d.getId());
                    break;
                }
            }
        } catch (DigitalOceanException | RequestUnsuccessfulException e) { }
    }
    
    @Override
    public final DropletType getType() {
        return (DropletType) super.getType();
    }
    
    //TODO: Check if a droplet with name is already create....
    @Override
    public void create() {
        if (id == null || id.equals(ServerController.NOT_CREATED_ID)) {
            Droplet toCreate = new Droplet();
            toCreate.setName(name);
            toCreate.setSize(getType().getRam());
            toCreate.setRegion(new Region(getType().getRegion()));
            toCreate.setImage(new Image(getType().getOperatingSystem()));
            
            toCreate.setEnableBackup(getType().isEnableBackup());
            toCreate.setEnableIpv6(getType().isEnableIpv6());
            toCreate.setEnablePrivateNetworking(getType().isEnablePrivateNetworking());
            
            if (getType().getCloudInit() != null) {
                Authentication auth = getAuthentication();
                String init = getType().getCloudInit().toString(auth);
                
                init = init.replace("{username}", getAuthentication().getUsername());
                
                if (auth != null) {
                    if (auth instanceof DflagAuth) {
                        DflagAuth dflag = (DflagAuth) auth;
                        if (!dflag.isValid()) {
                            throw new RuntimeException(String.format("Your DFlags are invalid. Please use -D%s with -D%s" /*either -D%s or -D%s"*/, DflagAuth.FLAG_USERNAME, DflagAuth.FLAG_PASSWORD/*, DflagAuth.FLAG_SSH_KEY*/));
                        }

//                        if (dflag.isSSHKey()) {
//                            init = init.replace("{ssh_key}", auth.getPassword());
//                            init = init.replace("{ssh}", auth.getPassword());
//                        }
                    } // else {
                        init = init.replace("{password}", auth.getPassword());
                        init = init.replace("{pass}", auth.getPassword());
                    }
//                }

                toCreate.setUserData(init);
            }
            
            try {
                int dId = this.client.createDroplet(toCreate).getId();
                this.id = String.valueOf(dId);
            } catch (DigitalOceanException | RequestUnsuccessfulException ex) {
                throw new RuntimeException(ex);
            }
        } else {
            if (!exists()) {
                throw new RuntimeException("A droplet with id of [" + id + "] does not exist. Please create it.");
            }
            
            //Maybe do something if we specify droplet id???? Come back later
        }
        
        onCreate();
    }
    
    /**
     * This should be called independently... After creation. 
     * 
     * Notes:
     *  1) Check if droplet isNew before
     *  2) Check if droplet is online before 
     */
    @Override
    public abstract void connect();
    
    @Override
    public boolean isConnected() {
        return getSSHController() != null && getSSHController().isConnected();
    }
    
    @Override
    public boolean exists() {
        Droplets droplets = null;
        
        try {
            droplets = this.client.getAvailableDroplets(1, null);
        } catch (DigitalOceanException | RequestUnsuccessfulException e) {
            throw new RuntimeException(e);
        }
        
        if (droplets == null) {
            return false;
        }
        
        for (Droplet droplet : droplets.getDroplets()) {
            if (droplet.getName().equalsIgnoreCase(name) || Objects.equals(droplet.getId(), Integer.parseInt(id))) {
                return true;
            }
        }
        
        return false;
    }
    
    public Droplet getDroplet() {
        if (id == null ? NOT_CREATED_ID == null : id.equals(NOT_CREATED_ID)) {
            return null; // Droplet not Created
        }
        
        Droplet d = null;
        try {
            d = this.client.getDropletInfo(Integer.parseInt(id));
        } catch (DigitalOceanException | RequestUnsuccessfulException e) {
            throw new RuntimeException(e);
        }
        
        return d;
    }
    
    @Override
    public boolean destroy() {
        if (id == null ? NOT_CREATED_ID == null : id.equals(NOT_CREATED_ID)) return false;
        
        try {
            this.client.deleteDroplet(Integer.parseInt(id));
            
            return true;
        } catch (DigitalOceanException | RequestUnsuccessfulException e) {
            throw new RuntimeException(e);
        }
    }
    
    public boolean isNew() {
        return getDroplet().isNew();
    }
    
    public boolean isActive() {
        return getDroplet().isActive();
    }
    
    public abstract void onCreate();
    
    public abstract void onConnect();
    
    protected abstract Authentication getAuthentication();
}
