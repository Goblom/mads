/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin.tasks;

import codes.goblom.mads.plugin.MadsBungeePlugin;
import java.util.concurrent.TimeUnit;
import lombok.RequiredArgsConstructor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.scheduler.ScheduledTask;

/**
 *
 * @author Bryan Larson
 */
@RequiredArgsConstructor
public class BungeeTask implements Runnable {
    private final MadsBungeePlugin plugin;
    private Runnable runnable;
    
    private ScheduledTask task;
    private boolean cancelled = false;
    
    public BungeeTask(MadsBungeePlugin plugin, Runnable task) {
        this(plugin);
        this.runnable = task;
    }
    
    @Override
    public void run() {
        if (runnable != null) {
            runnable.run();
        }
    }
    
    public boolean hasStarted() {
        return task != null && !cancelled;
    }
    
    public boolean isCancelled() {
        return task != null && cancelled;
    }
    
    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
        
        if (cancel && task != null) {
            task.cancel();
            this.task = null;
        }
    }
    
    public ScheduledTask unsafe() {
        return this.task;
    }
    
    public BungeeTask runAsync() {
        this.task = ProxyServer.getInstance().getScheduler().runAsync(plugin, this);
        this.cancelled = false;
        
        return this;
    }
    
    public BungeeTask schedule(long delay, TimeUnit unit) {
        this.task = ProxyServer.getInstance().getScheduler().schedule(plugin, this, delay, unit);
        this.cancelled = false;
        
        return this;
    }
    
    public BungeeTask schedule(long delay, long period, TimeUnit unit) {
        this.task = ProxyServer.getInstance().getScheduler().schedule(plugin, this, delay, period, unit);
        this.cancelled = false;
        
        return this;
    }
}
