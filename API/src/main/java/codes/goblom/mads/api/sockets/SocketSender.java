/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.sockets;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.ArrayList;

/**
 *
 * @author Bryan Larson
 */
public abstract class SocketSender {
    
    protected abstract Socket getSocket();
    
    public void send(Serializable obj) {
        try {
            OutputStream outputStream = getSocket().getOutputStream();
            ObjectOutputStream objectStream = new ObjectOutputStream(outputStream);
            
            objectStream.writeObject(obj);
            objectStream.flush();
        } catch (IOException e) { 
            e.printStackTrace();
        }
    }

    public void send(Serializable... objects) {
        if (objects == null || objects.length == 0) {
            return;
        }

        Serializable toSend = null;
        if (objects.length == 1) {
            toSend = objects[0];
        } else {
            ArrayList<Serializable> list = new ArrayList();
            for (Serializable o : objects) {
                if (o == null) {
                    continue;
                }
                list.add(o);
            }

            toSend = list;
        }

        if (toSend == null) {
            return;
        }
        
        send(toSend);
    }
}
