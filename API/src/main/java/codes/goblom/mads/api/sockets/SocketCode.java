/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.sockets;

import java.io.Serializable;

/**
 *
 * @author Bryan Larson
 */
public abstract class SocketCode implements Serializable {
    
    /**
     * Remotely stops the FSocketServer
     */
    public static final SocketCode CLOSE = new SocketCode() {
        @Override
        public void execute(SocketSender conn) {
            if (!(conn instanceof SocketConnection)) return;
            ((SocketConnection) conn).getSocketServer().stop();
        }
    };
    
    /**
     * Disconnects the FConnection
     */
    public static final SocketCode DISCONNECT = new SocketCode() {
        @Override
        public void execute(SocketSender conn) {
            if (!(conn instanceof SocketConnection)) return;
            ((SocketConnection) conn).close();
        }
    };
            
    public SocketCode() { }
    
    public abstract void execute(SocketSender conn);
}
