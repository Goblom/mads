/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.events.bridge.bukkit;

import codes.goblom.mads.api.sockets.SocketClient;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 *
 * @author Bryan Larson
 */
@RequiredArgsConstructor
@Getter
public class BridgeReceiveEvent extends Event {
    private static final HandlerList handlerList = new HandlerList();
    
    private final SocketClient client;
    private final Object received;

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
}
