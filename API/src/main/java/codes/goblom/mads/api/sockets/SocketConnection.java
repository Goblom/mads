/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.sockets;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 *
 * @author Bryan Larson
 */
public final class SocketConnection extends SocketSender implements Runnable {

    private final Socket socket;
    private final SocketServer socketServer;

    SocketConnection(Socket socket, SocketServer socketServer) {
        this.socket = socket;
        this.socketServer = socketServer;
    }

    @Override
    protected Socket getSocket() {
        return socket;
    }
    
    public SocketServer getSocketServer() {
        return socketServer;
    }
    
    @Override
    public void run() {
        while (!isClosed()) {
            try {
                InputStream inputStream = getSocket().getInputStream();
                if (inputStream.available() > 0) {
                    ObjectInputStream objectStream = new ObjectInputStream(inputStream);
                    Object recieved = objectStream.readObject();

                    if (recieved instanceof SocketCode) {
                        ((SocketCode) recieved).execute(this);
                        return;
                    }

                    if (socketServer.handler != null) {
                        socketServer.handler.onReceive(this, recieved);
                    } else {
                        System.out.println("ObjectReceiver is null. Skipping.");
                    }
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
    
    public boolean isClosed() {
        return getSocket() == null || getSocket().isClosed();
    }
    
    public void close() {
        if (!isClosed()) {
            try {
                getSocket().close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public InetAddress getAddress() {
        return getSocket().getInetAddress();
    }
}
