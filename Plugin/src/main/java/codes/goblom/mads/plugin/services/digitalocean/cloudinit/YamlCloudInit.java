/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin.services.digitalocean.cloudinit;

import codes.goblom.mads.api.services.digitalocean.CloudInit;
import codes.goblom.mads.api.auth.Authentication;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Bryan Larson
 */
@RequiredArgsConstructor
public class YamlCloudInit extends CloudInit {

    public static YamlCloudInit loadFromFile(Plugin plugin, String file) {
        YamlCloudInit init = new YamlCloudInit(plugin, file);
        init.load();
        return init;
    }
    
    public static YamlCloudInit loadFromFile(Plugin plugin, File file) {
        return loadFromFile(plugin, file.getName());
    }
    
    private final Plugin plugin;
    private final String file;
    
    private Configuration config;
    
    @Override
    public void load() {
        if (config == null) {
            if (!plugin.getDataFolder().exists()) {
                plugin.getDataFolder().mkdir();
            }

            File configFile = new File(plugin.getDataFolder(), file);
            if (!configFile.exists()) {
                try (InputStream in = plugin.getResourceAsStream(file)) {
                    Files.copy(in, configFile.toPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                this.config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        
        //TODO: PowerState
        if (config.contains("package_update")) setPackage_update(config.getBoolean("package_update"));
        if (config.contains("package_upgrade")) setPackage_upgrade(config.getBoolean("package_upgrade"));
        if (config.contains("packages")) setPackages(config.getStringList("packages"));
        if (config.contains("runcmd")) setRuncmd(config.getStringList("runcmd"));
        
        if (config.contains("chpasswd")) {
            ChangePassword chp = new ChangePassword();
            chp.setExpire(config.getBoolean("chpasswd.expire", false));
            List<String> list = config.getStringList("chpasswd.list");
            
            for (String str : list) {
                String[] s = str.split(":");
                String username = s[0];
                String password = s[1];
                
                chp.add(username, password);
            }
            
            setChpasswd(chp);
        }
        
        if (config.contains("groups")) {
            List groups = config.getList("groups");
            List<Group> list = new ArrayList();
            
            for (Object go : groups) {                 
                 if (go instanceof String) {
                     Group group = new Group();
                     group.setName(go.toString());
                     list.add(group);
                 } else if (go instanceof Map) {
                     Map<String, Object> map = (Map<String,Object>) go;
                     
                     for (String key : map.keySet()) {
                         Group group = new Group();
                         group.setName(key);
                         group.setDefaultUsers((List<String>) map.get(key));
                         
                         list.add(group);
                     }
                 }
            }
            setGroups(list);
        }
        
        if (config.contains("users")) {
            List<LinkedHashMap<String, Object>> users = (List<LinkedHashMap<String, Object>>) config.getList("users");
            List<User> list = new ArrayList();
            
            for (LinkedHashMap<String, Object> cu : users) {
                User user = new User();
                
                for (String key : new HashSet<String>(cu.keySet())) {
                    String cap = StringUtils.capitalize(key).replace("-", "_");
                    for (Method m : User.class.getDeclaredMethods()) {
                        if (m.getName().startsWith("set")) {
                            if (m.getName().endsWith(cap)) {
                                m.setAccessible(true);
                                try {
                                    
                                    switch (key.toLowerCase()) { //special cases
                                        case "sudo": //needs String not List
                                            user.setSudo(cu.get(key).toString());
                                            break;
                                        default:
                                            m.invoke(user, cu.get(key));
                                    }
                                    
                                    cu.remove(key);
                                    break;
                                } catch (Exception e) {
                                    e.printStackTrace();;
                                }
                            }
                        }
                    }
                }
                
                cu.keySet().forEach((key) -> {
                    System.out.println("Found remaining key '" + key + "' is it named correctly or supported?");
                });
                
                list.add(user);
            }
            
            setUsers(list);
        }
    }
    
    @Override
    public void save() {
        String saved = toString();
        if (!plugin.getDataFolder().exists()) {
            plugin.getDataFolder().mkdir();
        }

        File configFile = new File(plugin.getDataFolder(), file);
        try (FileWriter writer = new FileWriter(configFile);
            BufferedWriter bw = new BufferedWriter(writer)) {
            bw.write(saved);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return toString(null);
    }
    
    @Override
    public String toString(Authentication auth) {
        final String S = "    "; //spacer
        final String nl = "\n"; // System.lineSeparator();
        
        StringBuilder sb = new StringBuilder("#cloud-config");
        sb.append(nl);
        sb.append("package_upgrade: ").append(isPackage_upgrade()).append(nl);
        sb.append("package_update: ").append(isPackage_update()).append(nl);
        if (getPackages() != null && !getPackages().isEmpty()) {
            sb.append("packages:").append(nl);
            
            getPackages().forEach((p) -> {
                sb.append(S).append("- ").append(p).append(nl);
            });
        }
        
        //SecureAuth
        if (auth != null) {
            if (getUsers() == null) {
                setUsers(new ArrayList());
            }

            User secure = new User();
            secure.setName(auth.getUsername());
//            if (auth instanceof SecureAuth) { 
//                secure.setPasswd(auth.getPassword());
//            } else {
                secure.setPasswd("my_non_hashed_password_password");
//            }
            secure.setShell("/bin/bash");
            getUsers().add(secure);
        
            if (getChpasswd() == null) {
                setChpasswd(new ChangePassword());
            }

            if (getChpasswd().getList() == null) {
                getChpasswd().setList(new HashMap());
            }

//            if (!(auth instanceof SecureAuth)) {
                getChpasswd().getList().put(auth.getUsername(), auth.getPassword());
//            }
        }
        
        //Users
        if (getUsers() != null && !getUsers().isEmpty()) {
            sb.append("users:").append(nl);

            for (User u : getUsers()) {
                sb.append(S).append("- ");
                boolean first = true;
                
                //Strings
                String name = u.getName();
                String password = u.getPasswd();
                String sudo = u.getSudo();
                String shell = u.getShell();
//                String gecos = u.getGecos();
//                String home = u.getHomedir();
//                String expire = u.getExpiredate();
//                String primary = u.getPrimary_group();
//                String selinux = u.getSelinux_user();

                if (name != null && !name.isEmpty()) {
                    if (!first) sb.append(S + "  ");
                    first = false;
                    sb.append("name: ").append(name);
                    sb.append(nl);
                }
                if (password != null && !password.isEmpty()) {
                    if (!first) sb.append(S + "  ");
                    first = false;
                    sb.append("passwd: ").append(password);
                    sb.append(nl);
                }
                if (sudo != null && !sudo.isEmpty()) {
                    if (!first) sb.append(S + "  ");
                    first = false;
                    sb.append("sudo: '" + "  ").append(sudo).append("'");
                    sb.append(nl);
                }
                if (shell != null && !shell.isEmpty()) {
                    if (!first) sb.append(S + "  ");
                    first = false;
                    sb.append("shell: '").append(shell).append("'");
                    sb.append(nl);
                }
                
                //booleans
//                boolean inactive = u.isInactive();
//                boolean lock = u.isLock_password();
//                boolean noHome = u.isNo_create_home();
//                boolean nnoLog = u.isNo_log_init();
//                boolean noGroup = u.isNo_user_group();
//                boolean system = u.isSystem();
                
                //lists
//                List<String> sshKeys = u.getSsh_authorized_keys();
//                if (sshKeys != null && !sshKeys.isEmpty()) {
//                    boolean indent = false;
//                    if (!first) {
//                        sb.append(S + S);
//                        indent = true;
//                    }
//                    
//                    first = false;
//                    sb.append("ssh_authorized_keys:");
//                    sb.append(nl);
//                    
//                    for (String key : sshKeys) {
//                        sb.append(S + S);
//                        if (indent) sb.append(S);
//                        sb.append("- ").append(key);
//                        sb.append(nl);
//                    }
//                }
//                List<String> groups = u.getGroups();
            }
        }
        
        // Groups
        if (getGroups() != null && !getGroups().isEmpty()) {
            sb.append("groups:").append(nl);
            for (Group g : getGroups()) {
                sb.append(S).append("- ").append(g.getName());
                
                if (g.getDefaultUsers() != null && !g.getDefaultUsers().isEmpty()) {
                    sb.append(": [");
                    for (String u : g.getDefaultUsers()) {
                        sb.append(u).append(", ");
                    }
                    sb.append("]");
                }
                sb.append(nl);
            }
        }
        // chpassword
        if (getChpasswd() != null) {
            ChangePassword chp = getChpasswd();
            sb.append("chpasswd:").append(nl);
            
            if (chp.getList() != null && !chp.getList().isEmpty()) {
                sb.append(S).append("list: |").append(nl);
                chp.getList().entrySet().forEach((ent) -> {
                    sb.append(S + S);// .append("- ");
                    if (ent.getKey().startsWith("{")) sb.append("'"); //make sure its enclosed if special character.
                    sb.append(ent.getKey()).append(":").append(ent.getValue());
                    if (ent.getKey().startsWith("{")) sb.append("'"); //make sure its enclosed if special character.
                    sb.append(nl);
                });
            }
            sb.append(S).append("expire: ").append(chp.isExpire()).append(nl);
        }
        
        // runcmd
        if (getRuncmd() != null && !getRuncmd().isEmpty()) {
            sb.append("runcmd:");
            getRuncmd().forEach((s) -> {
                sb.append(S).append("- ").append(s).append(nl);
            });
        }
        
        String str = sb.toString()
                .replace(",]", "]")
                .replace(", ]", "]")
                .replace("''", "'")
                .trim();
        return str;
    }
}
