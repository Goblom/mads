/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin.services.local;

import codes.goblom.mads.api.ServerController;
import codes.goblom.mads.api.ServerType;
import codes.goblom.mads.api.events.MadsConnectEvent;
import codes.goblom.mads.api.services.DefaultServerType;
import codes.goblom.mads.api.services.local.LocalType;
import codes.goblom.mads.api.ssh.SSHController;
import codes.goblom.mads.api.ssh.impl.local.LocalSSHController;
import codes.goblom.mads.plugin.MadsBungeePlugin;
import codes.goblom.mads.api.utils.Utils;
import codes.goblom.mads.plugin.Permissions;
import codes.goblom.mads.plugin.tasks.OnlinePinger;
import codes.goblom.mads.plugin.utils.Messenger;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 *
 * @author Bryan Larson
 */
public class LServerController extends ServerController<LMadsImpl> {
    protected static final String LOCAL_SERVER_FOLDER = "/Local Servers/";
    
    private final MadsBungeePlugin plugin;
    
    private File folderDir;
    private Process serverProcess;
    private LocalSSHController sshController;
    
    public LServerController(MadsBungeePlugin plugin, LMadsImpl api, String name, ServerType type) {
        super(api, name, type);
        
        this.plugin = plugin;
    }

    @Override
    @Deprecated
    public String getId() {
        return getName();
    }
    
    @Override
    public void create() {
        if (serverProcess != null) return; //Server Already Started
        
        this.folderDir = new File(plugin.getDataFolder() + LOCAL_SERVER_FOLDER, getName());
        this.folderDir.mkdirs();
        
        //Do Init
        if (getType() != null) {
            List<String> init = getType().getInitCmds();
            
            if (init != null && !init.isEmpty()) {
                init.forEach((i) -> {
                    if (i.startsWith("@") || i.startsWith("$")) {
                        String cmd = i.substring(1).split(" ")[0];
                        String[] args = i.substring(cmd.length() + 1).trim().split(" ");
                        
                        switch (cmd.toLowerCase()) {
                            //Usage:
                            //      @write [file] [what to write]
                            //      @writer eula.txt eula=true
                            //      @writer server.properties online-mode=false
                            case "write":
                                try {
                                    String wFile = args[0];
                                    String wWrite = i.substring(cmd.length() + wFile.length() + 2).trim();

                                    FileWriter writer = new FileWriter(new File(folderDir, wFile));
                                    writer.write(wWrite);
                                    writer.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                
                                break;
                            // Usage:
                            //      @upload [file] (folder)
                            //      @copy   [file] (folder)
                            case "upload":
                            case "copy":
                                String sFile = args[0];
                                String sFolder = null;
                                
                                try {
                                    sFolder = args[1];
                                } catch (Exception e) { }
                                
                                File toCopy = new File(plugin.getDataFolder(), sFile);
                                File toFolder = null;
                                
                                if (sFolder == null) {
                                    toFolder = new File(folderDir, sFile);
                                } else {
                                    toFolder = new File(folderDir, sFolder);
                                }
                                
                                try {
                                    Utils.copyFile(toCopy, toFolder);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;
                            case "unzip":
                                plugin.getLogger().warning("Local MadsAPI does not yet support unzip. Please check back later.");
                                break;
                        }
                    } else {
                        try {
                            ProcessBuilder builder = new ProcessBuilder();
                                           builder.directory(folderDir.getAbsoluteFile());
                                           builder.command(i.split(" "));
                                           builder.start();
                        } catch (Exception e) {
                            plugin.getLogger().warning("There was an error running '" + i + "' from " + getType().getName());
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
        
        // Call ConnectEvent before server start
        //    So that plugins can do what they need before server start
        ProxyServer.getInstance().getPluginManager().callEvent(new MadsConnectEvent(this));

        //Start Server
        int port = Utils.findRandomPort(LMadsImpl.PORT_SCAN_START, LMadsImpl.PORT_SCAN_END);
        String launchArgs = null;
        
        if (getType() == null || getType() instanceof DefaultServerType) {
            launchArgs = "-jar server.jar";
        } else {
            launchArgs = ((LocalType) getType()).getLaunchArgs();
        }
        
        List<String> command = new ArrayList();
                     command.add("java");
                     command.addAll(Arrays.asList(launchArgs.split(" ")));
                     command.add("--port");
                     command.add(String.valueOf(port));
                     command.add("--nogui");
                     
        ProcessBuilder builder = new ProcessBuilder();
                       builder.directory(folderDir.getAbsoluteFile());
                       builder.command(command);
        
        try {
            System.out.println(builder);
            System.out.println(builder.command());
            
            serverProcess = builder.start();
        } catch (IOException e) {
            e.printStackTrace();
            
            throw new RuntimeException(e);
        }
        
        for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
            if (Permissions.NOTIFY.has(player) || Permissions.NOTIFY_CREATE.has(player)) {
                Messenger.sendMessage(player, "Server " + getName() + " was created.");
            }
        }
        
        //Online Pinger -- Add to Proxy
        String ip = "localhost"; //Maybe use Utils.getIpAddress() if this doesnt work?
        final ServerInfo info = ProxyServer.getInstance().constructServerInfo(getName(), new InetSocketAddress(ip, port), "", false);
        
        new OnlinePinger(plugin, info) {
            @Override
            public boolean onPing(ServerPing ping, Throwable error) {
                if (isCancelled() || error != null) return false;
                
                ComponentBuilder join = new ComponentBuilder("/server " + info.getName())
                        .color(ChatColor.GOLD)
                        .event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/server " + info.getName()))
                        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText("Click to execute this command")));

                String m1 = "BungeeCords says " + getName() + " is online. Try it out!";
                
                for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
                    if (Permissions.NOTIFY.has(player) || Permissions.NOTIFY_ONLINE.has(player)) {
                        Messenger.sendMessage(player, m1);
                        Messenger.sendMessage(player, join);
                    }
                }
                
                plugin.getProxy().getServers().put(info.getName(), info);
                return true;
            }
        }.schedule(30, 30, TimeUnit.SECONDS);
    }

    @Override
    public void connect() { }

    @Override
    @Deprecated //Since it is local, we are always connected
    public boolean isConnected() {
        return true; 
    }

    @Override
    @Deprecated //Since it is local, we are always online
    public boolean isOnline() {
        return true;
    }

    @Override
    public boolean exists() {
        return folderDir.exists();
    }

    @Override
    public boolean destroy() {
        // Try and stop server
        if (serverProcess != null) {
            PrintWriter writer = new PrintWriter(serverProcess.getOutputStream());
            writer.write("stop");
            writer.flush();
        }
        
        try {
            serverProcess.destroy();
            serverProcess = null;
            return Utils.delete(folderDir);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return false;
    }

    @Override
    public List<String> getIpAddresses() {
        return Arrays.asList("localhost", Utils.getIpAddress());
    }

    @Override
    public SSHController getSSHController() {
        if (this.sshController == null) {
            this.sshController = new LocalSSHController(folderDir);
        }
        
        return this.sshController;
    }
}
