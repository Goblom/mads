/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.ssh.impl.local;

import codes.goblom.mads.api.ssh.SSHController;
import codes.goblom.mads.api.ssh.wrappers.WrappedExec;
import codes.goblom.mads.api.ssh.wrappers.WrappedSftp;
import java.io.File;
import lombok.Getter;

/**
 *
 * @author Bryan Larson
 */
public class LocalSSHController implements SSHController {

    @Getter
    private final File homeDir;
    
    @Getter
    private final WrappedExec exec;
    @Getter
    private final WrappedSftp sftp;
    
    public LocalSSHController(File homeDir) {
        this.homeDir = homeDir.getAbsoluteFile();
        this.exec = new LocalExec(this.homeDir);
        this.sftp = new LocalSftp(this.homeDir);
    }
    
    @Override
    public void connect() { 
        // Since this assumes we are copying files on local machine we dont do anything here
    }

    @Override
    public boolean isConnected() { 
        return true; //Since we are on local machine we are always connected
    }  
}
