/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin;

import codes.goblom.mads.api.Mads;
import codes.goblom.mads.api.ServerController;
import codes.goblom.mads.api.ServerType;
import codes.goblom.mads.api.sockets.SocketConnection;
import codes.goblom.mads.plugin.auth.SecureAuth;
import codes.goblom.mads.plugin.bridge.MadsBridge;
import codes.goblom.mads.plugin.exec.CommandContext;
import codes.goblom.mads.plugin.exec.CommandInfo;
import codes.goblom.mads.plugin.exec.CommandListener;
import codes.goblom.mads.plugin.exec.Executor;
import codes.goblom.mads.plugin.tasks.BungeeTask;
import codes.goblom.mads.plugin.tasks.OnlinePinger;
import codes.goblom.mads.plugin.utils.Messenger;
import com.google.common.collect.Lists;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.config.ServerInfo;

/**
 *
 * @author Bryan Larson
 */
public class MadsCommands implements CommandListener {
    
    @Getter
    private final MadsBungeePlugin owningPlugin;
    private final Executor exec;
    
    MadsCommands(MadsBungeePlugin plugin) {
        this.owningPlugin = plugin;
        this.exec = new Executor(plugin, "mads") {
            @Override
            public void sendHelp(CommandContext context) {
                List<String> available = new ArrayList();
                
                context.getExecutor().getRegisteredCommands().stream().filter((info) -> (info.permission().has(context.getSender()))).map((info) -> {
                    if (available.isEmpty()) {
                        available.add(ChatColor.GREEN + "" + ChatColor.BOLD + ChatColor.ITALIC + "" + ChatColor.UNDERLINE + "Available Commands: ");
                    }
                    StringBuilder desc = new StringBuilder().append(ChatColor.GOLD).append(ChatColor.BOLD).append(info.name());
                    if (info.description() != null && !info.description().isEmpty()) {
                        desc.append(ChatColor.RESET);
                        desc.append(ChatColor.GRAY).append(" - ").append(ChatColor.BLUE).append(ChatColor.ITALIC).append(info.description());
                    }
                    return desc;
                }).forEachOrdered((desc) -> {
                    available.add(desc.toString());
                });
                
                if (!available.isEmpty()) {
                    available.forEach((m) -> {
                        context.message(m + ChatColor.RESET);
                    });
                }
            }

            @Override
            public void onError(CommandSender sender, Throwable e) { 
                Messenger.sendMessage(sender, "There was a command error...", ChatColor.RED);
                Messenger.sendMessage(sender, e.getMessage(), ChatColor.RED);
                e.printStackTrace();
            }
        };
        this.exec.addExecutor(this);
        
        HoverEvent event = new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Sorry. Get permission next time.").italic(true).color(ChatColor.LIGHT_PURPLE).create());
        this.exec.setNoPermissionMessage(new ComponentBuilder("You don't have permission to do that.").color(ChatColor.RED).event(event));
        
        ProxyServer.getInstance().getPluginManager().registerCommand(plugin, exec);
        
    }
    
    public void addExternalCommands(CommandListener listener) {
        if (listener instanceof MadsCommands) {
            throw new UnsupportedOperationException("Cannot add MadsCommands again.");
        }
        
        this.exec.addExecutor(listener);
    }
    
    class StatusInfo {
        String name;
        String error;
        String ip;
        boolean connected;
        boolean bridge = false;
        boolean mcOnline;
    }
    
    @CommandInfo(
            name = "status",
            alias = { "list" },
            description = "Check the status of the servers.",
            permission = Permissions.STATUS
    )
    void status(CommandContext context) {
        if (context.isTabExecutor()) return;
        
        List<StatusInfo> status = new ArrayList();
        
        for (ServerController cont : Mads.getAllControllers()) {
            StatusInfo info = new StatusInfo();
            info.name = cont.getName();
            info.connected = cont.isConnected();
            
            try {
                info.ip = cont.getIpAddresses().get(0).toString();
            } catch (Exception e) {
                info.ip = "Not Assigned yet.";
                info.error = "Not online.";
            }
            
            ServerInfo sf = ProxyServer.getInstance().getServerInfo(cont.getName());
            if (sf == null) {
                if (info.error == null) info.error = "Not added to Proxy.";
            } else {            
                if (MadsBridge.isEnabled()) {
                    SocketConnection con = MadsBridge.getConnection(sf.getName());
                    if (con == null) {
                        info.bridge = false;
                    } else {
                        info.bridge = !con.isClosed();
                    }
                }
                
                new OnlinePinger(owningPlugin, sf) {
                    @Override
                    public boolean onPing(ServerPing ping, Throwable error) {
                        return info.mcOnline = error == null && ping != null;
                    }
                }.schedule(0, 1, TimeUnit.SECONDS);
            }
            status.add(info);
        }

        context.message("This can take a bit. Be patient.", ChatColor.RED);
        new BungeeTask(owningPlugin) {
            @Override
            public void run() {
                context.message("Server Statuses:", ChatColor.BOLD, ChatColor.GOLD);
                
                if (status.isEmpty()) {
                    context.message("No Server found.", ChatColor.RED);
                } else {
                    for (StatusInfo info : status) {
                        context.message("Name: " + info.name);
                        context.message("IP: " + info.ip);
                        if (info.error != null) {
                            context.message("Error: " + info.error);
                        } else {
                            context.message("SSH Connected: " + info.connected);
                            context.message("MC Online: " + info.mcOnline);
                            
                            if (MadsBridge.isEnabled()) {
                                context.message("Bridge Connected: " + info.bridge);
                            }
                        }
                        context.message("");
                    }
                }
            }
        }.schedule(10, TimeUnit.SECONDS);
    }
    
    @CommandInfo(
            name = "setup",
            description = "Setup a secure username and password",
            permission = Permissions.SETUP
    )
    void setup(CommandContext context) throws IOException, NoSuchPaddingException, IllegalBlockSizeException, InvalidKeySpecException, NoSuchAlgorithmException, InvalidKeyException, InvalidAlgorithmParameterException, ClassNotFoundException, FileNotFoundException, BadPaddingException, IllegalArgumentException, IllegalAccessException {
        if (SecureAuth.exists(owningPlugin)) {
            if (!context.hasArg(0) || context.getArg(0)== null || context.getArg(0).isEmpty()) {
                context.message("Please enter your password.", ChatColor.BOLD, ChatColor.RED);
                return;
            }
            
            if (context.isTabExecutor()) return;
            
            String password = context.getArg(0);
            try {
                owningPlugin.updateAuth(SecureAuth.load(owningPlugin, password));
                Mads.reload();
                
                context.message("SecureAuth loaded successfully.", ChatColor.GREEN);
            } catch (NoSuchFieldException ex) {
                context.message("The MadsApi that is loaded was not developed properly. Please contact the developer of the MadsAPI in use.");
            }
            
            return;
        }
        
        String username = context.getArg(0);
        if (!context.hasArg(0) || (username == null || username.isEmpty() || username.length() <= 3)) {
            context.message("Requires a [username]", ChatColor.RED);
            return;
        }
        
        String password = context.getArg(1);
        if (!context.hasArg(1) || password == null || password.isEmpty()) {
            context.message("Requires a [password]", ChatColor.RED);
            context.suggest("Suggestion: Password should be between 5 and 16 characters.", ChatColor.ITALIC, ChatColor.BLUE);
            return;
        }
        
        if (password.length() < 5 || password.length() > 16) {
            context.message("Password [" + password+ "] does not meet minimum requirments.");
            return;
        } else {
            context.suggest("Your information seems secure enough", ChatColor.ITALIC, ChatColor.BLUE);
        }
        
        if (context.argsLength() >= 5) { //egg
            context.suggest("okay you can stop now....", ChatColor.ITALIC, ChatColor.BLUE);
        }
        
        if (context.isTabExecutor()) return;
        
        SecureAuth auth = new SecureAuth(username, password);
        SecureAuth.save(owningPlugin, auth, password);
        
        try {
            owningPlugin.updateAuth(auth);
            
            context.message("Secure Auth Created...", ChatColor.BOLD, ChatColor.UNDERLINE, ChatColor.GOLD);
            context.message("Username: " + auth.getUsername(), ChatColor.GREEN);
            context.message("Password: " + auth.getPassword(), ChatColor.GREEN);
            context.message("Don't forget the password.",ChatColor.BOLD, ChatColor.UNDERLINE, ChatColor.GOLD);
            context.message("It will be required everytime MADS is loaded.", ChatColor.BOLD, ChatColor.UNDERLINE, ChatColor.GOLD);
        } catch (NoSuchFieldException e) {
            context.message("The MadsApi that is loaded was not developed properly. Please contact the developer of the MadsAPI in use.");
        }
    }
    
    @CommandInfo(
            name = "exec",
            alias = { "cmd" },
            description = "Execute a command on a server machine.",
            permission = Permissions.EXEC
    )
    void cmd(CommandContext context) {
        if (!context.hasArg(0)) {
            context.message("Requires a [name] to exec on", ChatColor.RED);
            return;
        }
        
        String serverName = context.getArg(0);
        ServerController cont = Mads.getControllerByName(serverName);
        
        if (cont == null) {
            cont = Mads.getControllerById(serverName);
        }
        
        if (cont == null) {
            if (serverName == null || serverName.isEmpty()) {
                context.message("Requires a [name]", ChatColor.UNDERLINE, ChatColor.BOLD, ChatColor.RED);
                Mads.getAllControllers().forEach((dt) -> {
                    context.suggest(dt.getName(), ChatColor.GOLD);
                });
            } else {
                List<String> names = Lists.transform(new ArrayList<>(Mads.getAllControllers()), ServerController::getName);
                List<String> matches = Executor.copyPartialMatches(names, serverName);
                
                if (matches == null || matches.isEmpty() || context.hasArg(1)) {
                    context.message("Server Not Found...", (context.isTabExecutor() ? ChatColor.BOLD : null), ChatColor.RED);
                } else {
                    matches.forEach((m) -> { context.suggest(m); });
                }
            }
            
            return;
        }
        
        if (!cont.isConnected()) {
            context.message("Server is not connected to ssh. Something went wrong?", ChatColor.RED);
            return;
        }
        
        if (!context.hasArg(1) || context.getArg(1).isEmpty()) {
            context.message("No command to execute", ChatColor.UNDERLINE, ChatColor.BOLD, ChatColor.RED);
            context.suggest("This can be any program supported on " + cont.getName() + "s server.", ChatColor.ITALIC, ChatColor.BLUE);
            return;
        }
        
        if (context.isTabExecutor()) return;
        
        String cmd = context.combineRemaining(1);
        List<String> ret = cont.getSSHController().getExec().execOutputOf(cmd);
        
        context.message("Successfully ran command.", ChatColor.GRAY);
        String msg = "Ouput:\n";
        msg = ret.stream().map((r) -> r + "\n").reduce(msg, String::concat);
        
        context.message(msg, ChatColor.GRAY);
    }
    
    @CommandInfo(
            name = "create",
            alias = { "cr" },
            description = "Create a Server.",
            permission = Permissions.CREATE
    )
    void create(CommandContext context) {                
        if (!context.hasArg(0)) {
            String pr = context.getArg(0);
            
            if (pr == null || pr.length() > 1 || pr.isEmpty()) {
                return;
            }
            
            context.suggest("!context.hasArg(0) unknown case...", ChatColor.GREEN);
            context.suggest("Please report to @DevGoblom on twitter", ChatColor.GREEN);
            return;
        }
        
        String serverName = context.getArg(0);
        ServerType type = Mads.getServerType(serverName);
        
        if (type == null) {
            if (!context.isTabExecutor()) {
                context.message("Unknown server type... " + serverName, ChatColor.RED);
                return;
            }
            
            if (serverName == null || serverName.isEmpty()) {
                context.suggest("Requires a [type]", ChatColor.UNDERLINE, ChatColor.BOLD, ChatColor.RED);
                Mads.getServerTypes().forEach((dt) -> {
                    context.suggest(dt.getName(), ChatColor.GOLD);
                });
            } else {
                List<String> names = Lists.transform(Mads.getServerTypes(), ServerType::getName);
                List<String> matches = Executor.copyPartialMatches(names, serverName);
                
                if (matches == null || matches.isEmpty() || context.hasArg(1)) {
                    context.suggest("Unknown server type...", (context.isTabExecutor() ? ChatColor.BOLD : null), ChatColor.RED);
                } else {
                    matches.forEach((m) -> { context.suggest(m); });
                }
            }
            
            return;
        }
        
        if (!context.hasArg(1) || context.getArg(1).isEmpty()) {
            context.message("Requires a [name].", ChatColor.UNDERLINE, ChatColor.BOLD, ChatColor.RED);
            context.suggest("This is what is associated with the proxy.", ChatColor.GOLD);
            return;
        }

        if (context.getArg(1).length() <= 3) {
            context.suggest("Suggestion: Choose a name with more than 3 letters.", ChatColor.ITALIC, ChatColor.BLUE);
        }
        
        if (context.isTabExecutor()) return;
        
        context.message("Server is being created... This may take a moment.", ChatColor.GRAY);
        final ServerController cont = Mads.buildController().name(context.getArg(1)).type(serverName).build();
        
        // Is this really needed?
        new BungeeTask(owningPlugin) {
            @Override
            public void run() {
                while (true) {
                    if (cont.isOnline()) {
                        context.message("Server is online. Running init. I may notify you later.");
                        break;
                    }

                    try {
                        Thread.sleep(TimeUnit.SECONDS.toMillis(10));
                    } catch (InterruptedException ex) {
                    }
                }
            }
        }.runAsync();
    }
    
    @CommandInfo(
            name = "destroy",
            alias = { "d" },
            description = "Destroys a Server. Warning: May not save progress.",
            permission = Permissions.DESTROY
    )
    void destroy(CommandContext context) {
        if (!context.hasArg(0)) {
            context.message("Requires a [name] to exec on", ChatColor.RED);
            return;
        }
        
        String serverName = context.getArg(0);
        ServerController cont = Mads.getControllerByName(serverName);
        
        if (cont == null) {
            cont = Mads.getControllerById(serverName);
        }
        
        if (cont == null) {
            if (serverName == null || serverName.isEmpty()) {
                context.message("Requires a [name]", ChatColor.UNDERLINE, ChatColor.BOLD, ChatColor.RED);
                Mads.getAllControllers().forEach((dt) -> {
                    context.suggest(dt.getName(), ChatColor.GOLD);
                });
            } else {
                List<String> names = Lists.transform(new ArrayList(Mads.getAllControllers()), (ServerController input) -> input.getName());
                List<String> matches = Executor.copyPartialMatches(names, serverName);
                
                if (matches == null || matches.isEmpty() || context.hasArg(1)) {
                    context.message("Server Not Found...", (context.isTabExecutor() ? ChatColor.BOLD : null), ChatColor.RED);
                } else {
                    matches.forEach((m) -> { context.suggest(m); });
                }
            }
            
            return;
        }

        if (context.isTabExecutor()) return;
        
        cont.destroy();
        Mads.removeController(serverName);
        ProxyServer.getInstance().getServers().remove(serverName);
        context.message("Server " + serverName + " destroyed successfully...", ChatColor.GREEN);
    }
}
