/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api;

import codes.goblom.mads.api.auth.Authentication;
import java.util.Collection;
import java.util.List;

/**
 *
 * 
 * @author Bryan Larson
 */
public abstract class MadsApi<C extends ServerController> {
    private static MadsApi instance;
    
    public static final MadsApi getInstance() {
        return instance;
    }
    
    public static final void setInstance(MadsApi api) {
        if (api == null) {
            System.out.println("MadsApi Disabled...");
            instance = null;
            return;
        }
        
        if (instance != null) {
            throw new UnsupportedOperationException("MadsApi already setup.");
        }
        
        instance = api;
    }
    
    public abstract void reload();
    
    public abstract boolean addServerType(ServerType type);
    
    public abstract List<ServerType> getServerTypes();
    
    public abstract ServerType getServerType(String name);
    
    public abstract void removeServerType(String name);
    
    public abstract C getControllerById(String id);
    
    public abstract C getControllerByName(String name);
    
    public abstract void removeController(String name);
    
    public abstract Collection<C> getAllControllers();
    
    public final ServerBuilder buildController() {
        return new ServerBuilder(this);
    }
    
    protected abstract C createController(ServerBuilder cont);
    
    protected Authentication getAuthentication() {
        throw new UnsupportedOperationException("getAuthentication was not overridden in the API instance.");
    }
}
