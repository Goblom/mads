/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.utils;

/**
 *
 * @author Bryan Larson
 */
public final class ThreadStarter {
    public static final Starter DEFAULT = (runnable) -> {
        new Thread(runnable).start();
    };
        
    private static Starter starter = DEFAULT;
    
    public static void setStarter(Starter starter) {
        ThreadStarter.starter = starter;
    }
    
    public static void start(Runnable r) {
        if (starter == null) return;
        
        starter.start(r);
    }
    
    public static interface Starter {
        public void start(Runnable r);
    }
}
