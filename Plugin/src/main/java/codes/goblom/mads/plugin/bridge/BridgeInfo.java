/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin.bridge;

import codes.goblom.mads.api.sockets.SocketClient;
import codes.goblom.mads.api.sockets.SocketCode;
import codes.goblom.mads.api.sockets.SocketSender;

/**
 *
 * @author Bryan Larson
 */
public class BridgeInfo {
    
    public static SocketCode TRY_SHUTDOWN = new SocketCode() {
        @Override
        public void execute(SocketSender conn) {
            if (!(conn instanceof SocketClient)) return;
            
            try {
                org.bukkit.Bukkit.shutdown();
            } catch (Exception e) { e.printStackTrace(); }
        }
        
    };
    
    protected static final String CHANNEL = "mads:bridge";

    public static final int DEFAULT_PORT = 35565;
    
    protected static final String BRIDGE_PLUGIN_CONFIG = 
            "IP: {ip}\n" +
            "Port: {port}";
}
