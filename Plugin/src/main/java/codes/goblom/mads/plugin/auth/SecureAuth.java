/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin.auth;

import codes.goblom.mads.api.auth.Authentication;
import codes.goblom.mads.plugin.utils.Crypt;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import lombok.Getter;
import net.md_5.bungee.api.plugin.Plugin;

/**
 *
 * @author Bryan Larson
 */
public final class SecureAuth implements Authentication, Serializable {
//    private static final long serialVersionUID = 4L;
    private static final String SECURE_FILE = "auth.dat";
    
    private Plugin plugin;
    
    @Getter
//    @Setter(AccessLevel.PRIVATE)
    private String username;
    
    @Getter
//    @Setter(AccessLevel.PRIVATE)
    private String password;
    
    public SecureAuth() { }
    
    public SecureAuth(String username, String password) {
        this.username = username;
        this.password = password;
    }
    
    private SecureAuth attach(Plugin plugin) {
        this.plugin = plugin;
        return this;
    }
    
    public static boolean exists(Plugin plugin) {
        return new File(plugin.getDataFolder(), SECURE_FILE).exists();
    }
    
    public static void save(SecureAuth auth, String password) throws IOException, NoSuchPaddingException, IllegalBlockSizeException, InvalidKeySpecException, NoSuchAlgorithmException, InvalidKeyException, InvalidAlgorithmParameterException {
        if (auth.plugin != null) {
            save(auth.plugin, auth, password);
            return;
        }
        
        throw new UnsupportedOperationException("There is no plugin attached to SecureAuth");
    }
    
    public static void save(Plugin plugin, SecureAuth auth, String password) throws IOException, NoSuchPaddingException, IllegalBlockSizeException, InvalidKeySpecException, NoSuchAlgorithmException, InvalidKeyException, InvalidAlgorithmParameterException {
        Crypt.encrypt(auth, new File(plugin.getDataFolder(), SECURE_FILE), password);
    }
    
    public static Authentication load(Plugin plugin, String password) throws IOException, ClassNotFoundException, FileNotFoundException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, NoSuchAlgorithmException, InvalidKeyException, InvalidAlgorithmParameterException {
        return ((SecureAuth) Crypt.decrypt(new File(plugin.getDataFolder(), SECURE_FILE), password)).attach(plugin);
    }
}
