/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin.bridge;

import codes.goblom.mads.api.events.bridge.bukkit.BridgeReceiveEvent;
import codes.goblom.mads.api.sockets.SocketClient;
import codes.goblom.mads.api.utils.ThreadStarter;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

/**
 *
 * @author Bryan Larson
 */
public class BridgePlugin extends JavaPlugin implements Listener, PluginMessageListener {
    
    private static SocketClient client;
    boolean pollStarted = false;
    
    public static SocketClient getClient() {
        return client;
    }
    
    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(this, this);
                
        //Just in case
        ThreadStarter.setStarter((r) -> {
            Bukkit.getScheduler().runTaskAsynchronously(this, r);
        });
        
        try {
            YamlConfiguration config = YamlConfiguration.loadConfiguration(new File(getDataFolder(), "bridge.cfg"));
            String ip = config.getString("IP");
            int port = config.getInt("Port");
            
            client = new SocketClient(ip, port);
            poll();
        } catch (Exception e) { }
        
        
        // We dont like this because it requires a player to join... That is not fun.
        if (client == null) {
            Bukkit.getMessenger().registerIncomingPluginChannel(this, BridgeInfo.CHANNEL, this);
            Bukkit.getMessenger().registerOutgoingPluginChannel(this, BridgeInfo.CHANNEL);
        }
        
        getLogger().info("Bridge Loaded...");
    }
//
//    @EventHandler
//    public void onChat(AsyncPlayerChatEvent event) {
//        if (client == null) return;
//        
//        client.send("Player [" + event.getPlayer().getName() + "] sent message [" + event.getMessage() + "]");
//    }
    
    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {        
        try (DataInputStream in = new DataInputStream(new ByteArrayInputStream(message))) {
            int port = Integer.MIN_VALUE;
            String host = null;
            
            while (in.available() > 0) {
                String str;
                try {
                    str = in.readUTF();
                } catch (IOException e) {
                    e.printStackTrace();
                    continue;
                }
                
                if (client == null) {
                    String[] entry = str.split(",");
                    
                    for (String ent : entry) {
                        String[] strs = ent.split("-");
                        
                        if (strs[0].equalsIgnoreCase("port")) {
                            port = Integer.parseInt(strs[1]);
                        } else if (strs[0].equalsIgnoreCase("host")) {
                            host = strs[1];
                        }
                    }
                    if (host != null && port != Integer.MIN_VALUE) {
                        final String fHost = host;
                        final int fPort = port;
                        
                        Runnable r = () -> {
                            try {
                                client = new SocketClient(fHost, fPort);
                                poll();
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                        };
                        
                        ThreadStarter.start(r);
                        break;
                    }
                } else {
                    // Some other reason we would need PluginMessaging here?
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void poll() {
        if (client == null || pollStarted) return;
        pollStarted = true;
        
        Runnable r = () -> {
            while (true) {
                Object received = null;
                
                try {
                    received = client.readNext();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                
                if (received == null) continue;
                
                Bukkit.getPluginManager().callEvent(new BridgeReceiveEvent(client, received));
            }
        };
        
        ThreadStarter.start(r);
    }
}
