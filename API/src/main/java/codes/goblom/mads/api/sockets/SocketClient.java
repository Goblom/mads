/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.sockets;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 *
 * @author Bryan Larson
 */
public class SocketClient extends SocketSender {
    
    public static void main(String[] args) throws UnknownHostException, IOException, TimeoutException, ClassNotFoundException {
        System.out.println("Start a client.");
        SocketClient client = new SocketClient(InetAddress.getLocalHost(), 5556);

        System.out.println("Please type something to send to the server...");

        Scanner inputScanner = new Scanner(System.in);
        
        while (true) {
            String str = inputScanner.next();
            
            if (str.equals("disconnect") || str.equalsIgnoreCase("exit")) {
                break;
            }
            
            Random r = new Random();
            String obj = "Serialized Object - " + r.nextInt();
            System.out.println(obj);
            client.send(obj);

            System.out.println("Got the following message from the server:");
            System.out.println("Recieved: " + client.read(5, TimeUnit.SECONDS));
        }

        client.disconnect();
    }
    
    private final Socket connectedTo;
    
    public SocketClient(InetAddress addr, int port) throws IOException {
        this.connectedTo = new Socket(addr, port);
    }
    
    public SocketClient(String host, int port) throws IOException {
        this.connectedTo = new Socket(host, port);
    }

    @Override
    protected Socket getSocket() {
        return connectedTo;
    }
    
    /*
     * This function blocks.
     */
    public Object readNext() throws ClassNotFoundException {
        try {
            InputStream stream = getSocket().getInputStream();
            Object read = null;
            
            while (read == null) {
                if (stream.available() > 0) {
                    read = new ObjectInputStream(stream).readObject();
                }
            }
            
            if (read instanceof SocketCode) {
                ((SocketCode) read).execute(this);
                return null;
            }
            
            return read;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
        
    /*
     * This function blocks.
     */
    public Object read(long timeout, TimeUnit unit) throws TimeoutException, ClassNotFoundException {
        try {
            InputStream stream = getSocket().getInputStream();
            Object read = null;
            final long start = System.currentTimeMillis();
            final long millisTimeout = unit.toMillis(timeout);
            
            while (read == null) {
                long diff = System.currentTimeMillis() - start;
                
                if (diff > millisTimeout) {
                    throw new TimeoutException();
                }
                
                if (stream.available() > 0) {
                    read = new ObjectInputStream(stream).readObject();
                }
            }
            
            if (read instanceof SocketCode) {
                ((SocketCode) read).execute(this);
                return null;
            }
            
            return read;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public void disconnect() {
        try {
            send(SocketCode.DISCONNECT);
            if (getSocket() != null && !getSocket().isClosed()) {
                getSocket().close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
