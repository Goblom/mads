/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.plugin;

import java.util.ArrayList;
import java.util.List;
import net.md_5.bungee.api.CommandSender;


/**
 *
 * @author Bryan Larson
 */
public enum Permissions {
    ALL {
        @Override
        public boolean has(CommandSender sender) {
            return sender.hasPermission("mads.all") || sender.hasPermission("mads.*");
        }
    },
    CREATE,
    DESTROY,
    EXEC,
    SETUP,
    STATUS,
    NOTIFY_CREATE,
    NOTIFY_CONNECT,
    NOTIFY_UPLOAD,
    NOTIFY_ONLINE,
    NOTIFY("notify.*", NOTIFY_CREATE, NOTIFY_CONNECT, NOTIFY_UPLOAD, NOTIFY_ONLINE)
    ;
    
//    static {
//        for (Permissions main : Permissions.values()) {
//            String mainName = main.name();
//            if (!mainName.contains("_")) continue;
//            String parent = mainName.split("_")[0];
//            
//            for (Permissions test : Permissions.values()) {
//                if (test == main) continue;
//                if (test.name().equals(parent)) {
//                    test.permissions.addAll(main.permissions);
//                }
//            }
//        }
//    }
    
    private final List<String> permissions;
    
    private Permissions() {
        this.permissions = new ArrayList();
        this.permissions.add(toString().replace("_", "."));
    }
    
    private Permissions(Object... other) {
        this();
        
        for (Object o : other) {
            if (o instanceof String) {
                permissions.add("mads." + ((String) o).replace("_", "."));
            } else if (o instanceof Permissions) {
                permissions.addAll(((Permissions) o).permissions);
            }
        }
    }
    
    public boolean has(CommandSender sender) {
        boolean has = ALL.has(sender);
        
        if (!has) {
            for (String perm : permissions) {
                if (has) break;
                has = sender.hasPermission(perm);
            }
        }
        
        return has;
//        return 
//                sender.hasPermission("mads." + toString().toLowerCase().replace("_", ".")) ||
//                ALL.has(sender);
    }
}
