/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.ssh.impl.local;

import codes.goblom.mads.api.ssh.SSHController;
import codes.goblom.mads.api.ssh.UploadProgressMonitor;
import codes.goblom.mads.api.ssh.UploadProgressMonitor.UploadStatus;
import codes.goblom.mads.api.ssh.wrappers.WrappedSftp;
import codes.goblom.mads.api.utils.Utils;
import java.io.File;
import java.io.IOException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

/**
 *
 * @author Bryan Larson
 */
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class LocalSftp implements WrappedSftp {
    private final File homeDir;
    
    @Override
    public void upload(File file) {
        try {
            Utils.copyFile(file, new File(homeDir, file.getName()));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    @Deprecated //We dont support UploadProgressMonitor, but we dot call the Finished method
    public void upload(File file, UploadProgressMonitor monitor) {
        if (monitor != null) {
            monitor.accept(UploadStatus.STARTED);
        }
        
        upload(file);
        
        if (monitor != null) {
            monitor.accept(UploadStatus.FINISHED);
        }
    }

    @Override
    public void upload(File file, String dir) {
        try {
            Utils.copyFile(file, new File(homeDir, dir));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    @Deprecated //We dont support UploadProgressMonitor, but we dot call the Finished method
    public void upload(File file, String dir, UploadProgressMonitor monitor) {
        if (monitor != null) {
            monitor.accept(UploadStatus.STARTED);
        }
        
        upload(file, dir);
        
        if (monitor != null) {
            monitor.accept(UploadStatus.FINISHED);
        }
    }

    @Override
    @Deprecated
    public void attach(SSHController controller) { }

    @Override
    @Deprecated
    public void close(SSHController controller) { }

    @Override
    @Deprecated
    public boolean isReady() {
        return true;
    }
    
}
