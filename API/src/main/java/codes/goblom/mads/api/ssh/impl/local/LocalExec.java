/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.ssh.impl.local;

import codes.goblom.mads.api.ssh.SSHController;
import codes.goblom.mads.api.ssh.wrappers.WrappedExec;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

/**
 *
 * @author Bryan Larson
 */

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class LocalExec implements WrappedExec {
    private final File homeDir;
    
    @Override
    public void exec(String exec) {
        try {
            ProcessBuilder builder = new ProcessBuilder();
                           builder.directory(homeDir);
                           builder.command(exec.split(" "));
                           builder.start();
        } catch (IOException | SecurityException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<String> execOutputOf(String exec) {
        try {
            ProcessBuilder builder = new ProcessBuilder();
                           builder.directory(homeDir);
                           builder.command(exec.split(" "));
            Process process = builder.start();
            
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader err = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            
            List<String> list = new ArrayList();
            String l;
            
            while ((l = in.readLine()) != null) {
                list.add(l);
            }
            while ((l = err.readLine()) != null) {
                list.add(l);
            }
            
            return list;
        } catch (IOException | SecurityException ex) {
            ex.printStackTrace();
        }
        
        return Collections.emptyList();
    }

    @Override
    @Deprecated
    public void attach(SSHController controller) { }

    @Override
    @Deprecated
    public void close(SSHController controller) { }

    @Override
    @Deprecated
    public boolean isReady() {
        return true;
    }
}
