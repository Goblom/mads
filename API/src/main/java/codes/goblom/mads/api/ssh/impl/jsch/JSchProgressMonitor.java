/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.ssh.impl.jsch;

import codes.goblom.mads.api.ssh.UploadProgressMonitor;
import codes.goblom.mads.api.ssh.UploadProgressMonitor.UploadStatus;
import com.jcraft.jsch.SftpProgressMonitor;
import java.io.File;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Bryan Larson
 */
@Getter
public abstract class JSchProgressMonitor implements UploadProgressMonitor, SftpProgressMonitor {
    
    private long max = 0;
    private long count = 0;
    private long percent = 0;
    
    @Setter
    protected File file;
    
    @Override
    public final void init(int op, String src, String dest, long max) {
        if (max > 0) { //Is always -1 for some reason. Research later.
            this.max = max;
        } else {
            this.max = getFile().length();
        }
        
        accept(UploadStatus.STARTED);
    }

    @Override
    public final boolean count(long bytes) {
        this.count += bytes;
        long p = count * 100 / max;
        
        if (p > percent) {
            this.percent = p;
        }
        
        accept(UploadStatus.TICK);
        
        return true;
    }

    @Override
    public final void end() {
        accept(UploadStatus.FINISHED);
    }
}
