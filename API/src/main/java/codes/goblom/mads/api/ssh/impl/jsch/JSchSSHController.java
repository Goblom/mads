/*
 * Copyright (C) 2020 Bryan Larson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codes.goblom.mads.api.ssh.impl.jsch;

import codes.goblom.mads.api.ServerController;
import codes.goblom.mads.api.auth.Authentication;
import codes.goblom.mads.api.ssh.SSHController;
import codes.goblom.mads.api.ssh.UploadProgressMonitor;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import java.io.File;
import java.util.List;
import java.util.Properties;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 *
 * @author Bryan Larson
 */
@RequiredArgsConstructor
public class JSchSSHController implements SSHController {
    public static int AUTH_ATTEMPTS = 5;
    
    protected final ServerController serverController;
    protected final Authentication auth;
    
    protected JSch client;
    @Getter
    protected Session session;
    
    private JSchExec exec;
    
    @Override
    public void connect() {
        if (client == null) {
            client = new JSch();
            
//            if (auth instanceof DflagAuth) {
//                if (((DflagAuth) auth).isSSHKey()) {
//                    try {
//                        client.addIdentity(auth.getPassword().trim().replace("'", ""));
//                    } catch (JSchException ex) {
//                        throw new RuntimeException(ex);
//                    }
//                }
//            }
        }
        
        int attempts = 0;
        while (session == null || !session.isConnected()) {
            try {
                Properties prop = new Properties();
                prop.put("StrictHostKeyChecking", "no");
                String ip = serverController.getIpAddresses().get(0).toString();
                this.session = client.getSession(auth.getUsername(), ip);

//                if (!(auth instanceof DflagAuth)) {
                    session.setPassword(auth.getPassword());
//                }

                session.setConfig(prop);
                session.connect();
            } catch (JSchException e) {
                attempts++;
                
                if (attempts >= AUTH_ATTEMPTS) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
    
    @Override
    public boolean isConnected() {
        return session != null && session.isConnected();
    }
    
    @Override
    public JSchExec getExec() {
        if (exec != null && exec.isReady()) {
            return exec;
        }
        
        this.exec = new JSchExec();
        attach(this.exec);
        
        return exec;
    }
    
    public List<String> execOutputOf(String exec) {
        return getExec().execOutputOf(exec);
    }
    
    public void exec(String exec) {
        getExec().exec(exec);
    }
    
    @Override
    public JSchSftp getSftp() {
        JSchSftp sftp = new JSchSftp();
        sftp.setUploadPath("/home/" + auth.getUsername() + "/");
        attach(sftp);
        return sftp;
    }
    
    public JSchSftp getSftp(String uploadDir) {
        JSchSftp sftp = new JSchSftp(uploadDir);
        attach(sftp);
        return sftp;
    }
    
    public void upload(File file, UploadProgressMonitor monitor) {
        getSftp().upload(file, monitor);
    }
    
    public void upload(File file, String dir, UploadProgressMonitor monitor) {
        getSftp().upload(file, dir, monitor);
    }
}
